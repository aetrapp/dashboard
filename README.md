# Aetrapp Dashboard

The web interface to explore [Aetrapp](https://aetrapp.org).

Check our [contributing guide](CONTRIBUTING.md).

## Installation and Usage

The steps below will walk you through setting up a development environment for the frontend.

### Install dependencies

Install the following on your system:

- [Git](https://git-scm.com)
- [nvm](https://github.com/creationix/nvm)
- [yarn](https://yarnpkg.com/docs/install)

Clone this repository locally and activate required Node.js version:

```
nvm install
```

Install Node.js dependencies:

```
yarn install
```

### Configuring

The following environment variables are available:

* API_URL: Aetrapp Data Service endpoint;
* SITE_URL: Dashboard public URL.

### Development

Start watcher to rebuild from changes automatically:

    yarn watch

Open a new terminal and start serving at [http://localhost:8000](http://localhost:8000) from `./public` folder:

    yarn serve

### Build to production

Run:

    yarn prod

This will generate a minified build in `./public` folder, pointing to production API URL.

## References & Related Repositories

- [API](https://github.com/aetrapp/api)
- [Mobile app](https://github.com/aetrapp/mobile)

## License

[GPL-3.0](LICENSE)
