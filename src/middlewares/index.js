import reduxThunk from 'redux-thunk';
import reduxPromiseMiddleware from 'redux-promise-middleware';
import { routerMiddleware } from 'react-router-redux';
import { browserHistory } from 'react-router';

export default [
  reduxThunk,
  reduxPromiseMiddleware(),
  routerMiddleware(browserHistory)
];
