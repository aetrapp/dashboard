import client from 'services/feathers';
import { authenticate } from 'actions/auth';

export default function init (store) {

  if(window.localStorage['feathers-jwt']) {
    store.dispatch(authenticate());
  }

}

// Utils

export function hasRole (auth, role) {
  if(auth.signedIn && Array.isArray(auth.user.roles)) {
    return auth.user.roles.indexOf(role) !== -1;
  } else {
    return false;
  }
}

export function hasUser(auth) {
  return auth.signedIn;
}
