import feathers from "feathers-client";
import { stringify } from "qs";
import axios from "axios";

const socketio = feathers.socketio;
const hooks = feathers.hooks;
const errors = feathers.errors; // An object with all of the custom error types.
const authentication = feathers.authentication;
const io = require("socket.io-client");

export const apiUrl = process.env.API_URL || "https://api.aetrapp.org";

const socket = io(apiUrl, {
  transports: ["websocket"]
});

const client = feathers()
  .configure(
    socketio(socket, {
      timeout: 60000
    })
  )
  .configure(hooks())
  .configure(
    authentication({
      storage: window.localStorage
    })
  );

export const restUrl = (service, params) => {
  return `${apiUrl}/${service}?${stringify(params)}`;
};

export const authenticatedRestGet = (service, params) => {
  return axios.get(restUrl(service, params), {
    headers: {
      Authorization: window.localStorage["feathers-jwt"]
    }
  });
};

export default client;
