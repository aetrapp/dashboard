export const statuses = {
  "waiting-sample": "Aguardando amostra",
  "above-average": "Acima da média",
  "below-average": "Abaixo da média",
  "no-eggs": "Sem ovos",
  delayed: "Atrasada",
  inactive: "Desativada"
};

const statusColors = {
  "waiting-sample": "#006AB8",
  "above-average": "#EB604D",
  "below-average": "#FAEC01",
  "no-eggs": "#059C3F",
  delayed: "#818181",
  inactive: "#010101"
};

export const getStatus = status => {
  return statuses[status];
};

export const getStatusColor = status => {
  return statusColors[status];
};

export const getAddressString = trap => {
  let str = "";
  if (trap.addressStreet) str += trap.addressStreet;
  if (trap.addressNumber) str += `, ${trap.addressNumber}`;
  if (trap.addressComplement) {
    str += ` - ${trap.addressComplement}`;
  }
  return str;
};
