import React, { Component } from "react";
import { withRouter, Switch, Route } from "react-router-dom";
import List from "./List.jsx";

class Samples extends Component {
  render() {
    const { match } = this.props;
    return (
      <Switch>
        <Route exact path={match.url} component={List} />
      </Switch>
    );
  }
}

export default withRouter(Samples);
