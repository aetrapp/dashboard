import _ from "underscore";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Divider, Button, Icon, Dimmer, Loader } from "semantic-ui-react";
import client, { apiUrl } from "services/feathers";
import SamplesFilter from "components/SamplesFilter.jsx";
import SamplesTable from "components/SamplesTable.jsx";
import DownloadButtons from "components/DownloadButtons.jsx";
import FilteredDownload from "components/FilteredDownload.jsx";

class ListTraps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      samples: [],
      query: {
        $sort: {
          collectedAt: -1
        },
        $limit: 10
      },
      total: 0,
      hasMore: false,
      page: 1
    };
    this._handleFilter = this._handleFilter.bind(this);
    this._updateSamples = this._updateSamples.bind(this);
    this._loadMore = this._loadMore.bind(this);
  }
  componentDidMount() {
    this._updateSamples();
  }
  componentDidUpdate(prevProps, prevState) {
    const { query } = this.state;
    if (JSON.stringify(query) !== JSON.stringify(prevState.query)) {
      this._updateSamples();
    }
  }
  _updateSamples() {
    const { query } = this.state;
    this.setState({ loading: true });
    let sendQuery = { ...query };
    if (sendQuery.$sort) {
      const key = Object.keys(sendQuery.$sort)[0];
      if (key && !sendQuery[key]) sendQuery[key] = { $ne: null };
    }
    client
      .service("samples")
      .find({ query: sendQuery })
      .then(res => {
        this.setState({
          loading: false,
          total: res.total,
          page: 1,
          samples: res.data
        });
        this._updatePaging(res, 1);
      })
      .catch(err => {
        this.setState({ loading: false });
        console.log(err);
      });
  }
  _handleFilter = _.debounce(filter => {
    const { query } = this.state;
    let newQuery = Object.assign({}, query);
    delete newQuery["status"];
    delete newQuery["$or"];
    delete newQuery["error"];
    if (filter.status) {
      if (filter.status == "errored") {
        newQuery["error"] = { $ne: null };
      } else {
        newQuery["status"] = filter.status;
      }
    }
    if (filter.error) {
      newQuery["$or"] = {};
      newQuery["$or"]["error.message"] = { $iLike: `%${filter.error}%` };
      newQuery["$or"]["id"] = { $iLike: `%${filter.error}%` };
    }
    this.setState({ query: newQuery });
  }, 200);
  _updatePaging(res, page = false) {
    if (!page) page = this.state.page;
    if (res.limit * page < res.total) {
      this.setState({
        hasMore: true
      });
    } else {
      this.setState({
        hasMore: false
      });
    }
  }
  _handleSort = field => () => {
    const { query } = this.state;
    const current = Object.keys(query.$sort)[0];
    const $sort = field
      ? {
          [field]: current == field ? -query.$sort[field] : 1
        }
      : {};
    this.setState({
      query: {
        ...query,
        $sort
      }
    });
  };
  _loadMore() {
    const { query, page } = this.state;
    client
      .service("samples")
      .find({
        query: Object.assign({}, query, {
          $skip: page * query.$limit
        })
      })
      .then(res => {
        this.setState({ page: page + 1 });
        this.setState({
          samples: [...this.state.samples, ...res.data]
        });
        this._updatePaging(res, page + 1);
      });
  }
  render() {
    const { loading, samples, query, total, hasMore } = this.state;
    return (
      <div>
        <SamplesFilter onChange={this._handleFilter} />
        <DownloadButtons />
        <FilteredDownload
          service="samples"
          query={query}
          basic
          size="tiny"
          icon
          floated="right"
          style={{ marginRight: "1rem" }}
        >
          <Icon name="download" /> Baixar resultados filtrados
        </FilteredDownload>
        <Button
          floated="right"
          basic
          size="tiny"
          icon
          onClick={this._updateSamples}
          style={{ marginRight: "1rem" }}
        >
          <Icon name="refresh" loading={loading} /> Atualizar resultados
        </Button>
        <p
          style={{
            float: "left",
            lineHeight: "30px",
            margin: 0
          }}
        >
          {total} amostras encontradas.
        </p>
        <Divider clearing hidden />
        <div style={{ position: "relative", minHeight: "300px" }}>
          <Dimmer active={loading} inverted>
            <Loader />
          </Dimmer>
          <SamplesTable
            samples={samples}
            onSort={this._handleSort}
            sort={query.$sort}
            full
          />
        </div>
        <Divider hidden />
        {hasMore ? (
          <Button fluid basic onClick={this._loadMore}>
            Carregar mais amostras
          </Button>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps)(ListTraps);
