import React, { Component } from "react";
import { withRouter, Switch, Route } from "react-router-dom";
import TrapsNav from "./Nav";
import ListTraps from "./List.jsx";
import EditTrap from "./Edit.jsx";
import SingleTrap from "./Single.jsx";

class Traps extends Component {
  render() {
    const { match } = this.props;
    return (
      <div>
        {/* <TrapsNav /> */}
        <Switch>
          <Route exact path={match.url} component={ListTraps} />
          <Route path={`${match.url}/edit/:id`} component={EditTrap} />
          <Route path={`${match.url}/:id`} component={SingleTrap} />
        </Switch>
      </div>
    );
  }
}

export default withRouter(Traps);
