import _ from "underscore";
import axios from "axios";
import React, { Component } from "react";
import client, { apiUrl } from "services/feathers";
import { connect } from "react-redux";
import { addNotification } from "actions/notifications";
import {
  Segment,
  Header,
  Form,
  Select,
  Dropdown,
  Input,
  Checkbox,
  File,
  Button,
  Divider
} from "semantic-ui-react";
import ImageInput from "components/ImageInput.jsx";
import MapInput from "components/map/MapInput.jsx";
import { Redirect } from "react-router-dom";
import { setWith, clone, pick } from "lodash";

class TrapsEdit extends Component {
  constructor(props) {
    super(props);
    this.service = client.service("traps");
    this.citiesService = client.service("cities");
    this.state = {
      citiesOptions: {},
      trap: {
        coordinates: {
          type: "Point",
          coordinates: [0, 0],
          crs: {
            type: "name",
            properties: {
              name: "EPSG:4326"
            }
          }
        }
      }
    };
    this._onSubmit = this._onSubmit.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this._handleCheckbox = this._handleCheckbox.bind(this);
  }
  componentDidMount() {
    const { match } = this.props;
    if (match.params.id) {
      this.setState({ loading: true });
      this.service
        .get(match.params.id)
        .then(trap => {
          this.setState({
            trap: {
              ...this.state.trap,
              ...trap,
              coordinates: {
                ...trap.coordinates,
                crs: {
                  type: "name",
                  properties: {
                    name: "EPSG:4326"
                  }
                }
              }
            },
            loading: false
          });
        })
        .catch(err => {
          console.warn(err);
        });
    }
  }
  componentWillReceiveProps(nextProps) {
    const { match } = this.props;
    if (nextProps.match !== match) {
      this.setState({
        trap: {}
      });
    }
  }
  _handleChange = (ev, { name, value }) => {
    let newTrap = Object.assign({}, this.state.trap);
    this.setState({
      trap: setWith(clone(newTrap), name, value, clone)
    });
  };
  _handleCheckbox = (ev, { name, value }) => {
    const checked = this.state.trap[name];
    this.setState({
      trap: {
        ...this.state.trap,
        [name]: !checked
      }
    });
  };
  _onSubmit() {
    const { addNotification } = this.props;
    const { trap } = this.state;
    if (trap.id) {
      this.service
        .patch(
          trap.id,
          pick(
            trap,
            "postcode",
            "addressStreet",
            "addressComplement",
            "neighbourhood",
            "coordinates",
            "isActive",
            "base64"
          )
        )
        .then(data => {
          addNotification({
            message: "Alterações salvas com sucesso.",
            level: "success"
          });
        })
        .catch(err => {
          addNotification({
            message: "Erro: " + err.message,
            level: "error"
          });
        });
    }
  }
  render() {
    const { auth, match } = this.props;
    const { trap, citiesOptions } = this.state;
    return (
      <Segment>
        <Header as="h3">
          {!trap.id ? (
            <span>Novo usuário</span>
          ) : (
            <span>Editando armadilha {trap.id}</span>
          )}
        </Header>
        <Form onSubmit={this._onSubmit}>
          <Form.Field
            control={Input}
            label="CEP"
            placeholder="CEP"
            name="postcode"
            onChange={this._handleChange}
            value={trap.postcode}
          />
          <Form.Group widths="equal">
            <Form.Field
              control={Input}
              label="Endereço"
              placeholder="Endereço"
              name="addressStreet"
              onChange={this._handleChange}
              value={trap.addressStreet}
            />
            <Form.Field
              control={Input}
              label="Complemento"
              placeholder="Complemento"
              name="addressComplement"
              onChange={this._handleChange}
              value={trap.addressComplement}
            />
            <Form.Field
              control={Input}
              label="Bairro"
              placeholder="Bairro"
              name="neighbourhood"
              onChange={this._handleChange}
              value={trap.neighbourhood}
            />
          </Form.Group>
          <Form.Field
            control={MapInput}
            label="Coordenadas"
            name="coordinates.coordinates"
            onChange={this._handleChange}
            value={trap.coordinates.coordinates}
          />
          <Divider />
          <Form.Field
            control={Checkbox}
            label="Armadilha ativa"
            name="isActive"
            checked={trap.isActive}
            onChange={this._handleCheckbox}
          />
          <Divider />
          <Form.Field
            control={ImageInput}
            label="Enviar nova imagem de armadilha"
            name="base64"
            onChange={this._handleChange}
            defaultValue={`${apiUrl}/files/${trap.imageId}`}
          />
          <Button fluid primary type="submit">
            Enviar
          </Button>
        </Form>
        {!auth.signedIn ? <Redirect to="/" /> : null}
      </Segment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = {
  addNotification
};

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(TrapsEdit);
