import _ from "underscore";
import { stringify } from "qs";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Divider, Button, Icon, Dimmer, Loader } from "semantic-ui-react";
import client from "services/feathers";
import TrapsFilter from "components/TrapsFilter.jsx";
import TrapsTable from "components/TrapsTable.jsx";
import DownloadButtons from "components/DownloadButtons.jsx";
import FilteredDownload from "components/FilteredDownload.jsx";

class ListTraps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      traps: [],
      loading: false,
      query: {
        $sort: {
          createdAt: -1
        },
        $limit: 10
      },
      total: 0,
      hasMore: false,
      page: 1,
      editing: {}
    };
    this._handleFilter = this._handleFilter.bind(this);
    this._updateTraps = this._updateTraps.bind(this);
    this._loadMore = this._loadMore.bind(this);
  }
  componentDidMount() {
    this._updateTraps();
  }
  componentDidUpdate(prevProps, prevState) {
    const { query } = this.state;
    if (JSON.stringify(query) !== JSON.stringify(prevState.query)) {
      this._updateTraps();
    }
  }
  _updateTraps() {
    const { query } = this.state;
    this.setState({
      loading: true
    });
    let sendQuery = { ...query };
    if (sendQuery.$sort) {
      const key = Object.keys(sendQuery.$sort)[0];
      if (key && !sendQuery[key]) sendQuery[key] = { $ne: null };
    }
    client
      .service("traps")
      .find({ query: sendQuery })
      .then(res => {
        this.setState({
          loading: false,
          page: 1,
          total: res.total,
          traps: res.data
        });
        this._updatePaging(res, 1);
      })
      .catch(err => {
        this.setState({
          loading: false
        });
        console.log(err);
      });
  }
  _handleFilter = _.debounce(filter => {
    const { query } = this.state;
    let newQuery = Object.assign({}, query);
    delete newQuery["$or"];
    delete newQuery["cityId"];
    delete newQuery["status"];
    if (filter.q) {
      newQuery["$or"] = {};
      newQuery["$or"]["addressStreet"] = { $iLike: `%${filter.q}%` };
      newQuery["$or"]["id"] = { $iLike: `%${filter.q}%` };
    }
    if (filter.status) {
      newQuery["status"] = filter.status;
    }
    if (filter.cities && filter.cities.length) {
      newQuery["cityId"] = { $in: filter.cities };
    }
    this.setState({ query: newQuery });
  }, 200);
  _updatePaging(res, page = false) {
    if (!page) page = this.state.page;
    if (res.limit * page < res.total) {
      this.setState({
        hasMore: true
      });
    } else {
      this.setState({
        hasMore: false
      });
    }
  }
  _handleSort = field => () => {
    const { query } = this.state;
    const current = Object.keys(query.$sort)[0];
    const $sort = field
      ? {
          [field]: current == field ? -query.$sort[field] : 1
        }
      : {};
    this.setState({
      query: {
        ...query,
        $sort
      }
    });
  };
  _loadMore() {
    const { query, page } = this.state;
    client
      .service("traps")
      .find({
        query: Object.assign({}, query, {
          $skip: page * query.$limit
        })
      })
      .then(res => {
        this.setState({ page: page + 1 });
        this.setState({
          traps: [...this.state.traps, ...res.data]
        });
        this._updatePaging(res, page + 1);
      });
  }
  render() {
    const { traps, loading, total, hasMore, query, editing } = this.state;
    return (
      <div>
        <TrapsFilter onChange={this._handleFilter} />
        <DownloadButtons />
        <FilteredDownload
          service="traps"
          query={query}
          basic
          size="tiny"
          icon
          floated="right"
          style={{ marginRight: "1rem" }}
        >
          <Icon name="download" /> Baixar resultados filtrados
        </FilteredDownload>
        <Button
          floated="right"
          basic
          size="tiny"
          icon
          onClick={this._updateTraps}
          style={{ marginRight: "1rem" }}
        >
          <Icon name="refresh" loading={loading} /> Atualizar resultados
        </Button>
        <p
          style={{
            float: "left",
            lineHeight: "30px",
            margin: 0
          }}
        >
          {total} armadilhas encontradas.
        </p>
        <Divider hidden clearing />
        <div style={{ position: "relative", minHeight: "300px" }}>
          <Dimmer active={loading} inverted>
            <Loader />
          </Dimmer>
          <TrapsTable
            traps={traps}
            onSort={this._handleSort}
            sort={query.$sort}
            showUser
          />
        </div>
        <Divider hidden />
        {hasMore ? (
          <Button fluid basic onClick={this._loadMore}>
            Carregar mais armadilhas
          </Button>
        ) : null}
      </div>
    );
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps)(ListTraps);
