import React, { Component } from 'react';
import { Link } from 'react-router-dom';
import { Menu, Icon } from 'semantic-ui-react';
import Restrict from "components/Restrict.jsx";

class Nav extends Component {
  render () {
    return (
      <Restrict roles="admin">
        <Menu secondary>
          <Menu.Item
            as={Link}
            to="/traps/edit"
            name="new"
            >
              <Icon name="plus" />
              Cadastrar nova armadilha
            </Menu.Item>
          </Menu>
      </Restrict>
    )
  }
}

export default Nav;
