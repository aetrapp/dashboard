import React, { Component } from "react";
import _ from "underscore";
import { connect } from "react-redux";
import PropTypes from "prop-types";
import {
  Divider,
  Image,
  Button,
  Segment,
  Grid,
  Message,
  Statistic,
  Item,
  Icon,
  Header,
  Label
} from "semantic-ui-react";
import { Link, Redirect } from "react-router-dom";
import {
  ResponsiveContainer,
  ComposedChart,
  Line,
  Area,
  XAxis,
  YAxis,
  Tooltip,
  Legend
} from "recharts";
import {
  FacebookShareButton,
  TwitterShareButton,
  TelegramShareButton,
  WhatsappShareButton
} from "react-share";
import moment from "moment";
moment.locale("pt-br");
import SimpleMap from "components/SimpleMap";
import SamplesTable from "components/SamplesTable.jsx";
import TrapsModeration from "components/TrapsModeration.jsx";
import TrapStatus from "components/TrapStatus.jsx";
import client, { apiUrl } from "services/feathers";
import { sortBy } from "underscore";
import { getStatus, getAddressString } from "lib/traps";

const SampleTooltip = ({ active, label, payload }) => {
  if (active) {
    if (
      payload.length == 1 &&
      (payload[0].dataKey == "cityAvgEggCount" ||
        payload[0].dataKey == "extendedCityAvgEggCount")
    ) {
      return (
        <Label color="black">
          {payload.map(item => {
            if (item.dataKey == "cityAvgEggCount") {
              return (
                <p key={item.dataKey}>
                  Média da semana: {item.value} <br />
                  Semana do dia {moment(item.payload.date).format("DD/MM")}
                </p>
              );
            } else {
              return null;
            }
          })}
        </Label>
      );
    } else {
      return (
        <Label color="black">
          {moment.unix(label).fromNow()}
          {payload.map(item => {
            if (item.dataKey == "sampleEggCount") {
              return <p key={item.dataKey}>{item.value} ovo(s) encontrados</p>;
            } else if (item.dataKey == "cityAvgEggCount") {
              return (
                <p key={item.dataKey}>Média de {item.value} ovo(s) na cidade</p>
              );
            }
          })}
        </Label>
      );
    }
  } else {
    return null;
  }
};

const CityAvgLabel = ({ value, refData, ...props }) => {
  const data = refData[props.index];
  if (value && data.firstOfWeek) {
    return (
      <text
        x={props.x}
        y={props.y}
        className="recharts-text recharts-label"
        textAnchor="left"
        fontSize=".7em"
        style={{
          fill: "rgba(0,0,0,0.5)"
        }}
      >
        <tspan x={props.x + 5} dy="-1em">
          {moment(data.date).format("DD/MM")}:{" "}
          <tspan style={{ fontWeight: "bold", color: "#000" }}>{value}</tspan>
        </tspan>
      </text>
    );
    return <p>{value}</p>;
  } else {
    return null;
  }
};

const SampleLabel = ({ value, ...props }) => {
  if (value) {
    return (
      <text
        x={props.x}
        y={props.y}
        className="recharts-text recharts-label"
        textAnchor="middle"
        fontSize=".8em"
      >
        <tspan x={props.x} dy="-.75em">
          {value} ovos
        </tspan>
      </text>
    );
    return <p>{value}</p>;
  } else {
    return null;
  }
};

class SingleTrap extends Component {
  constructor(props) {
    super(props);
    this.state = {
      removed: false,
      trap: false,
      query: {
        trapId: props.match.params.id,
        $sort: {
          collectedAt: -1
        }
      },
      samples: []
    };
    this._handleModeration = this._handleModeration.bind(this);
  }
  componentDidMount() {
    const { match } = this.props;
    const { query } = this.state;
    // Fetch trap
    client
      .service("traps")
      .get(match.params.id)
      .then(trap => {
        this.setState({
          trap
        });
      })
      .catch(err => {
        console.log(err);
      });
    // Fetch samples
    client
      .service("samples")
      .find({ query })
      .then(res => {
        this.setState({
          samples: res.data
        });
      })
      .catch(err => {
        console.log(err);
      });
  }
  componentDidUpdate(prevProps, prevState) {
    const { query } = this.state;
    if (JSON.stringify(prevState.query) !== JSON.stringify(query)) {
      client
        .service("samples")
        .find({ query })
        .then(res => {
          this.setState({
            samples: res.data
          });
        })
        .catch(err => {
          console.log(err);
        });
    }
  }
  _userCanEdit() {
    const { trap } = this.state;
    const { auth } = this.props;
    if (!auth.signedIn) return false;
    if (auth.user.roles && auth.user.roles.indexOf("admin") !== -1) {
      return true;
    } else if (
      auth.user.roles &&
      auth.user.roles.indexOf("moderator") !== -1 &&
      auth.user.cities &&
      auth.user.cities.length
    ) {
      const ids = auth.user.cities.map(city => city.id);
      return ids.indexOf(trap.addressCityId) !== -1;
    }
  }
  _handleModeration(type, data) {
    switch (type) {
      case "removed":
        this.setState({
          removed: true
        });
        break;
      case "updated":
        this.setState({
          trap: data
        });
        break;
    }
  }
  _getNextSampleDate(trap) {
    return moment(trap.cycleStart)
      .add(trap.cycleDuration + 1, "days")
      .fromNow();
  }
  _trapLastCount(trap) {
    if (trap.lastSample && Number.isInteger(trap.lastSample.eggCount)) {
      return trap.lastSample.eggCount;
    } else if (Number.isInteger(trap.eggCount)) {
      return trap.eggCount;
    }
    return "--";
  }
  _trapLastDate(trap) {
    if (trap.lastSample) {
      return moment(trap.lastSample.collectedAt).fromNow();
    } else {
      return null;
    }
  }
  _hasChart(trap) {
    return this.state.samples.length;
  }
  _cityAvgs() {
    const { trap } = this.state;
    let avgs = [];
    if (trap.city.eggCountAverages) {
      for (const week in trap.city.eggCountAverages) {
        avgs.push({
          week,
          eggCountAverage: trap.city.eggCountAverages[week]
        });
      }
    }
    return avgs;
  }
  _composeChart() {
    const cityAvgs = this._cityAvgs();
    const { samples } = this.state;
    let data = [];
    for (const cityAvg of cityAvgs) {
      let parsedDate = cityAvg.week.split("-");
      let start = moment()
        .year(parsedDate[0])
        .week(parsedDate[1])
        .startOf("week");
      let end = moment()
        .year(parsedDate[0])
        .week(parsedDate[1])
        .endOf("week");
      data.push({
        date: start.toDate(),
        time: start.unix(),
        cityAvgEggCount: cityAvg.eggCountAverage,
        firstOfWeek: true
      });
      data.push({
        date: end.toDate(),
        time: end.unix(),
        cityAvgEggCount: cityAvg.eggCountAverage
      });
    }
    for (const sample of samples) {
      data.push({
        date: moment(sample.collectedAt).toDate(),
        time: moment(sample.collectedAt).unix(),
        sampleEggCount: sample.eggCount
      });
    }
    const lastSample = _.max(
      _.filter(data, item => Number.isInteger(item.sampleEggCount)),
      item => item.time
    );
    const lastCityAvg = _.max(
      _.filter(data, item => Number.isInteger(item.cityAvgEggCount)),
      item => item.time
    );
    if (lastSample.date > lastCityAvg.date) {
      data.push({
        date: lastCityAvg.date,
        time: lastCityAvg.time,
        extendedCityAvgEggCount: lastCityAvg.cityAvgEggCount
      });
      data.push({
        date: lastSample.date,
        time: lastSample.time,
        extendedCityAvgEggCount: lastCityAvg.cityAvgEggCount
      });
    }
    data = sortBy(data, item => item.date);
    return data;
  }
  _ticks() {
    let samples = [...this.state.samples];
    let ticks = [];
    if (samples && samples.length) {
      samples = sortBy(samples, item => new Date(item.collectedAt));
      ticks = samples.map(sample => moment(sample.collectedAt).unix());
    }
    return ticks;
  }
  _handleSort = field => () => {
    const { query } = this.state;
    const current = Object.keys(query.$sort)[0];
    this.setState({
      query: {
        ...query,
        $sort: {
          [field]: current == field ? -query.$sort[field] : 1
        }
      }
    });
  };
  render() {
    const { trap, query, samples, removed } = this.state;
    const url = location.href;
    if (removed) {
      return <Redirect to="/traps" />;
    }
    if (trap) {
      const hasChart = this._hasChart(trap);
      const ticks = this._ticks();
      const chartData = this._composeChart();
      return (
        <div>
          <SimpleMap
            height="400px"
            trap={trap}
            coordinates={trap.coordinates.coordinates}
          />
          <Item.Group>
            <Item>
              <Item.Content>
                <Grid>
                  <Grid.Row>
                    {trap.imageId ? (
                      <Grid.Column width="4">
                        <Image src={`${apiUrl}/files/${trap.imageId}`} />
                      </Grid.Column>
                    ) : null}
                    <Grid.Column width="12">
                      <Button.Group floated="right" size="big">
                        <Button
                          as={FacebookShareButton}
                          url={url}
                          icon="facebook"
                          color="facebook"
                        />
                        <Button
                          as={TwitterShareButton}
                          url={url}
                          icon="twitter"
                          color="twitter"
                        />
                        <Button
                          as={TelegramShareButton}
                          url={url}
                          icon="telegram"
                          color="instagram"
                        />
                        <Button
                          as={WhatsappShareButton}
                          url={url}
                          icon="whatsapp"
                          color="green"
                        />
                      </Button.Group>
                      <Item.Header>
                        <Header as="h2" size="huge" style={{ maxWidth: "70%" }}>
                          {getAddressString(trap)}
                        </Header>
                        <Label.Group>
                          {/* <Label color={trap.isActive ? "green" : null}>
                            {trap.isActive ? "Ativa" : "Concluída"}
                          </Label> */}
                          <Label image>
                            <TrapStatus status={trap.status} />
                            {getStatus(trap.status)}
                            <Label.Detail>Situação</Label.Detail>
                          </Label>
                          {this._userCanEdit() ? (
                            <Label
                              as={Link}
                              image
                              to={`/users/${trap.ownerId}`}
                            >
                              {trap.owner.firstName} {trap.owner.lastName}
                              <Label.Detail>Autor</Label.Detail>
                            </Label>
                          ) : null}
                        </Label.Group>
                      </Item.Header>
                      <Divider />
                      <Item.Meta>
                        {trap.city ? (
                          <p>
                            <Icon name="point" />
                            <strong>
                              {trap.city.name}, {trap.city.stateId}
                            </strong>{" "}
                            {trap.postcode ? (
                              <Label size="tiny">
                                <strong>CEP</strong>: {trap.postcode}
                              </Label>
                            ) : null}
                          </p>
                        ) : null}
                        <Divider />
                        <p>
                          <Icon name="calendar" /> cadastrada em{" "}
                          {moment(trap.createdAt).format("DD/MM/YYYY HH:mm")},
                          próxima coleta {this._getNextSampleDate(trap)}
                        </p>
                      </Item.Meta>
                    </Grid.Column>
                  </Grid.Row>
                </Grid>
                <Divider clearing hidden />
                <Item.Description>
                  {trap.sampleCount && trap.sampleCount ? (
                    <Grid centered columns={2}>
                      <Grid.Row>
                        <Grid.Column width="12">
                          <Segment.Group>
                            <Segment>
                              <p>
                                Foram realizadas {trap.sampleCount} amostras
                                nesta armadilha
                              </p>
                            </Segment>
                            {hasChart ? (
                              <Segment>
                                <Header as="h3">Série histórica</Header>
                                <ResponsiveContainer height={300}>
                                  <ComposedChart data={chartData}>
                                    <Line
                                      type="monotone"
                                      dataKey="sampleEggCount"
                                      stroke="#8884d8"
                                      strokeWidth="2"
                                      dot={{ strokeWidth: 5 }}
                                      name="Amostras"
                                      label={<SampleLabel />}
                                      connectNulls
                                    />
                                    <Area
                                      type="stepAfter"
                                      dataKey="cityAvgEggCount"
                                      fill="#e06565"
                                      stroke="#e06565"
                                      legendType="square"
                                      name="Média da cidade na semana"
                                      activeDot={false}
                                      label={
                                        <CityAvgLabel refData={chartData} />
                                      }
                                      connectNulls
                                    />
                                    <Area
                                      type="stepAfter"
                                      dataKey="extendedCityAvgEggCount"
                                      fill="#e06565"
                                      fillOpacity="0.2"
                                      stroke="#e06565"
                                      strokeDasharray="2,2"
                                      legendType="none"
                                      activeDot={false}
                                      label={false}
                                      connectNulls
                                    />
                                    <XAxis
                                      type="number"
                                      scale="time"
                                      dataKey="time"
                                      domain={["dataMin", "dataMax"]}
                                      padding={{ left: 0, right: 50 }}
                                      interval={0}
                                      ticks={ticks}
                                      tickFormatter={tick =>
                                        moment.unix(tick).format("L")
                                      }
                                    />
                                    <YAxis domain={[0, "dataMax + 10"]} />
                                    <Tooltip content={<SampleTooltip />} />
                                    <Legend
                                      verticalAlign="bottom"
                                      height={36}
                                    />
                                  </ComposedChart>
                                </ResponsiveContainer>
                              </Segment>
                            ) : null}
                          </Segment.Group>
                          {samples.length ? (
                            <Segment>
                              <Header as="h3" size="large">
                                Amostras
                              </Header>
                              <SamplesTable
                                samples={samples}
                                onSort={this._handleSort}
                                sort={query.$sort}
                              />
                            </Segment>
                          ) : null}
                        </Grid.Column>
                        <Grid.Column width={hasChart ? "4" : "4"}>
                          <Segment>
                            <Statistic>
                              <Statistic.Value>
                                {this._trapLastCount(trap)}
                              </Statistic.Value>
                              <Statistic.Label>
                                ovos encontrados na última amostra
                              </Statistic.Label>
                            </Statistic>
                          </Segment>
                          <Message
                            icon
                            size="tiny"
                            color={
                              trap.status == "delayed" ||
                              trap.status == "inactive"
                                ? "grey"
                                : "green"
                            }
                          >
                            <Icon
                              name={
                                trap.status == "delayed" ||
                                trap.status == "inactive"
                                  ? "cancel"
                                  : "checkmark"
                              }
                            />
                            <Message.Content>
                              Última amostra realizada{" "}
                              {this._trapLastDate(trap)}, próxima coleta{" "}
                              {this._getNextSampleDate(trap)}
                            </Message.Content>
                          </Message>
                        </Grid.Column>
                      </Grid.Row>
                    </Grid>
                  ) : (
                    <p
                      style={{
                        fontSize: "1.5em",
                        padding: "2rem 0",
                        color: "#ccc",
                        textAlign: "center",
                        fontStyle: "italic"
                      }}
                    >
                      Não há registro de amostras para esta armadilha
                    </p>
                  )}
                </Item.Description>
                <Divider hidden />
                <TrapsModeration
                  trap={trap}
                  onChange={this._handleModeration}
                />
              </Item.Content>
            </Item>
          </Item.Group>
        </div>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps)(SingleTrap);
