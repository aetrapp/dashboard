import React, { Component } from "react";
import _ from "underscore";
import { connect } from "react-redux";
import { Header, Segment, Divider, Button, Icon } from "semantic-ui-react";
import client, { apiUrl } from "services/feathers";
import { Redirect } from "react-router-dom";
import TrapsFilter from "components/TrapsFilter.jsx";
import TrapsTable from "components/TrapsTable.jsx";
import UserInfoTable from "components/UserInfoTable.jsx";
import FilteredDownload from "components/FilteredDownload.jsx";

class UsersSingle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      user: false,
      query: {
        ownerId: props.match.params.id,
        $limit: 10,
        $sort: {
          createdAt: -1
        }
      },
      total: 0,
      hasMore: false,
      page: 1,
      traps: []
    };
    this._loadMore = this._loadMore.bind(this);
  }
  componentDidMount() {
    const { match } = this.props;
    const { query } = this.state;
    client
      .service("users")
      .get(match.params.id)
      .then(user => {
        this.setState({ user });
      });
    client
      .service("traps")
      .find({ query })
      .then(res => {
        this.setState({
          traps: res.data,
          total: res.total,
          page: 1
        });
        this._updatePaging(res, 1);
      })
      .catch(err => {
        console.log(err);
      });
  }
  componentDidUpdate(prevProps, prevState) {
    const { query } = this.state;
    if (JSON.stringify(prevState.query) !== JSON.stringify(query)) {
      client
        .service("traps")
        .find({ query })
        .then(res => {
          this.setState({
            traps: res.data,
            total: res.total,
            page: 1
          });
          this._updatePaging(res, 1);
        })
        .catch(err => {
          console.log(err);
        });
    }
  }
  _handleFilter = _.debounce(filter => {
    const { query } = this.state;
    let newQuery = Object.assign({}, query);
    delete newQuery["$or"];
    delete newQuery["cityId"];
    delete newQuery["status"];
    if (filter.q) {
      newQuery["$or"] = {};
      newQuery["$or"]["addressStreet"] = { $iLike: `%${filter.q}%` };
      newQuery["$or"]["id"] = { $iLike: `%${filter.q}%` };
    }
    if (filter.status) {
      newQuery["status"] = filter.status;
    }
    if (filter.cities && filter.cities.length) {
      newQuery["cityId"] = { $in: filter.cities };
    }
    this.setState({ query: newQuery });
  }, 200);
  _updatePaging(res, page = false) {
    if (!page) page = this.state.page;
    if (res.limit * page < res.total) {
      this.setState({
        hasMore: true
      });
    } else {
      this.setState({
        hasMore: false
      });
    }
  }
  _handleSort = field => () => {
    const { query } = this.state;
    const current = Object.keys(query.$sort)[0];
    this.setState({
      query: {
        ...query,
        $sort: {
          [field]: current == field ? -query.$sort[field] : 1
        }
      }
    });
  };
  _loadMore() {
    const { query, page } = this.state;
    client
      .service("traps")
      .find({
        query: Object.assign({}, query, {
          $skip: page * query.$limit
        })
      })
      .then(res => {
        this.setState({ page: page + 1 });
        this.setState({
          traps: [...this.state.traps, ...res.data]
        });
        this._updatePaging(res, page + 1);
      });
  }
  render() {
    const { auth } = this.props;
    const { user, query, hasMore, total, traps, editing } = this.state;
    if (user) {
      return (
        <div>
          <Header as="h3" size="huge">
            {user.firstName} {user.lastName}
          </Header>
          <UserInfoTable user={user} />
          <Segment>
            <TrapsFilter onChange={this._handleFilter} />
            <FilteredDownload
              service="traps"
              query={query}
              basic
              size="tiny"
              icon
              floated="right"
            >
              <Icon name="download" /> Baixar resultados filtrados
            </FilteredDownload>
            <p
              style={{
                float: "left",
                lineHeight: "30px",
                margin: 0
              }}
            >
              {total} armadilhas encontradas.
            </p>
            <Divider hidden clearing />
            <TrapsTable
              traps={traps}
              onSort={this._handleSort}
              sort={query.$sort}
            />
          </Segment>
          <Divider hidden />
          {hasMore ? (
            <Button fluid basic onClick={this._loadMore}>
              Carregar mais armadilhas
            </Button>
          ) : null}
          {!auth.signedIn ? <Redirect to="/" /> : null}
        </div>
      );
    } else {
      return null;
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps)(UsersSingle);
