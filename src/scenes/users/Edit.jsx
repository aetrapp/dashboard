import _ from "underscore";
import axios from "axios";
import React, { Component } from "react";
import client, { apiUrl } from "services/feathers";
import { connect } from "react-redux";
import { addNotification } from "actions/notifications";
import {
  Segment,
  Header,
  Form,
  Select,
  Dropdown,
  Input,
  Checkbox,
  Button
} from "semantic-ui-react";
import { Redirect } from "react-router-dom";

class UsersEdit extends Component {
  constructor(props) {
    super(props);
    this.service = client.service("users");
    this.citiesService = client.service("cities");
    this.state = {
      citiesOptions: {},
      user: {
        isActive: true
      }
    };
    this._onSubmit = this._onSubmit.bind(this);
    this._getUserAllowedCities = this._getUserAllowedCities.bind(this);
    this._searchCities = this._searchCities.bind(this);
    this._afterSubmit = this._afterSubmit.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this._handleCheckbox = this._handleCheckbox.bind(this);
    this._hasCreated = this._hasCreated.bind(this);
  }
  componentDidMount() {
    const { match } = this.props;
    if (match.params.id) {
      this.setState({ loading: true });
      this.service
        .get(match.params.id)
        .then(user => {
          this.setState({
            user,
            loading: false
          });
          this._getUserAllowedCities();
        })
        .catch(err => {
          console.warn(err);
        });
    }
  }
  componentWillReceiveProps(nextProps) {
    const { match } = this.props;
    if (nextProps.match !== match) {
      this.setState({
        user: {
          isActive: true
        }
      });
    }
  }
  _searchCities = _.debounce((ev, data) => {
    if (data.searchQuery) {
      this.citiesService
        .find({
          query: {
            name: { $iLike: `%${data.searchQuery}%` }
          }
        })
        .then(res => {
          const cities = {};
          res.data.forEach(city => {
            cities[city.id] = {
              key: city.id,
              value: city.id,
              text: `${city.name} - ${city.stateId}`
            };
          });
          this.setState({
            citiesOptions: {
              ...this.state.citiesOptions,
              ...cities
            }
          });
        });
    }
  }, 200);
  _handleChange = (e, { name, value }) => {
    this.setState({
      user: {
        ...this.state.user,
        [name]: value
      }
    });
  };
  _handleCheckbox = (e, { name, value }) => {
    const checked = this.state.user[name];
    this.setState({
      user: {
        ...this.state.user,
        [name]: !checked
      }
    });
  };
  _onSubmit() {
    const { addNotification } = this.props;
    const { user } = this.state;
    if (user.id) {
      this.service
        .patch(
          user.id,
          _.omit(user, "email", "isVerified", "verifyToken", "resetToken")
        )
        .then(data => {
          addNotification({
            message: "Alterações salvas com sucesso.",
            level: "success"
          });
          this._afterSubmit(data);
        })
        .catch(err => {
          addNotification({
            message: "Erro: " + err.message,
            level: "error"
          });
        });
    } else {
      this.service
        .create(user)
        .then(data => {
          this.setState({ user: data });
          addNotification({
            message: "Usuário criado com sucesso.",
            level: "success"
          });
          this._afterSubmit(data);
        })
        .catch(err => {
          addNotification({
            message: "Erro: " + err.message,
            level: "error"
          });
        });
    }
  }
  _getAllowedCitiesPath(user) {
    return `${apiUrl}/users/${user.id}/allowedCities`;
  }
  _getUserAllowedCities() {
    const { user } = this.state;
    if (user.id) {
      const path = this._getAllowedCitiesPath(user);
      axios.get(path).then(res => {
        const cities = {};
        res.data.forEach(city => {
          cities[city.id] = {
            key: city.id,
            value: city.id,
            text: `${city.name} - ${city.stateId}`
          };
        });
        this.setState({
          citiesOptions: {
            ...this.state.citiesOptions,
            ...cities
          },
          user: {
            ...this.state.user,
            allowedCities: res.data.map(city => city.id)
          }
        });
      });
    }
  }
  _afterSubmit(data) {
    const path = this._getAllowedCitiesPath(data);
    const { user } = this.state;
    // Cities relationship
    axios.get(path).then(res => {
      const currentIds = res.data.map(city => city.id);
      const newIds = user.allowedCities;
      const idsToRemove = _.difference(currentIds, newIds);
      const idsToAdd = _.difference(newIds, currentIds);
      idsToRemove.forEach(cityId => {
        axios.delete(path, { params: { cityId } });
      });
      idsToAdd.forEach(cityId => {
        axios.post(path, { cityId });
      });
    });
  }
  _hasCreated() {
    const { match } = this.props;
    const { user } = this.state;
    if (!match.params.id && user.id) {
      return true;
    } else {
      return false;
    }
  }
  render() {
    const { auth, match } = this.props;
    const { user, citiesOptions } = this.state;
    return (
      <Segment>
        {this._hasCreated() ? <Redirect to={`/users/edit/${user.id}`} /> : null}
        <Header as="h3">
          {!user.id ? (
            <span>Novo usuário</span>
          ) : (
            <span>
              Editando {user.firstName} {user.lastName}
            </span>
          )}
        </Header>
        <Form onSubmit={this._onSubmit}>
          <Form.Group widths="equal">
            <Form.Field
              control={Input}
              label="Primeiro nome"
              placeholder="Primeiro nome"
              name="firstName"
              onChange={this._handleChange}
              value={user.firstName}
            />
            <Form.Field
              control={Input}
              label="Sobrenome"
              placeholder="Sobrenome"
              name="lastName"
              onChange={this._handleChange}
              value={user.lastName}
            />
          </Form.Group>
          <Form.Group widths="equal">
            {!user.id ? (
              <Form.Field
                control={Input}
                label="Email"
                placeholder="Email"
                name="email"
                onChange={this._handleChange}
                value={user.email}
              />
            ) : null}
            <Form.Field
              control={Input}
              label="Telefone residencial"
              placeholder="Telefone residencial"
              name="landlineNumber"
              onChange={this._handleChange}
              value={user.landlineNumber}
            />
            <Form.Field
              control={Input}
              label="Celular"
              placeholder="Celular"
              name="cellphoneNumber"
              onChange={this._handleChange}
              value={user.cellphoneNumber}
            />
          </Form.Group>
          <Form.Group widths="equal">
            <Form.Field
              control={Input}
              label="Data de nascimento"
              placeholder="Data de nascimento"
              name="dateOfBirth"
              onChange={this._handleChange}
              value={user.dateOfBirth}
            />
            <Form.Field
              control={Select}
              label="Gênero"
              placeholder="Gênero"
              name="gender"
              onChange={this._handleChange}
              value={user.gender}
              options={[
                {
                  text: "Não declarado",
                  value: ""
                },
                {
                  text: "Masculino",
                  value: "male"
                },
                {
                  text: "Feminino",
                  value: "female"
                },
                {
                  text: "Outro",
                  value: "other"
                }
              ]}
            />
          </Form.Group>
          {!user.id ? (
            <Form.Group widths="equal">
              <Form.Field
                control={Input}
                type="password"
                label="Senha"
                placeholder="Senha"
                name="password"
                onChange={this._handleChange}
              />
              <Form.Field
                control={Input}
                type="password"
                label="Repetir senha"
                placeholder="Repetir senha"
                name="password_repeat"
                onChange={this._handleChange}
              />
            </Form.Group>
          ) : null}
          <Form.Field
            control={Select}
            multiple
            label="Função"
            name="roles"
            placeholder="Função"
            value={user.roles || []}
            onChange={this._handleChange}
            options={[
              {
                text: "Moderador",
                value: "moderator"
              },
              {
                text: "Administrador",
                value: "admin"
              }
            ]}
          />
          {user.roles && user.roles.indexOf("moderator") !== -1 ? (
            <Form.Field
              control={Dropdown}
              fluid
              search
              selection
              multiple
              label="Região de moderação"
              name="allowedCities"
              value={user.allowedCities || []}
              onChange={this._handleChange}
              onSearchChange={this._searchCities}
              options={Object.values(citiesOptions)}
            />
          ) : null}
          <Form.Field
            control={Checkbox}
            label={{ children: "Usuário ativo" }}
            name="isActive"
            onChange={this._handleCheckbox}
            checked={user.isActive}
          />
          <Button fluid primary type="submit">
            Enviar
          </Button>
        </Form>
        {!auth.signedIn ? <Redirect to="/" /> : null}
      </Segment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = {
  addNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(UsersEdit);
