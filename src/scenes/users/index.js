import React, { Component } from "react";
import { withRouter, Switch, Route } from "react-router-dom";
import UsersList from "components/UsersList.jsx";
import UsersEdit from "./Edit.jsx";
import UsersSingle from "./Single.jsx";

class Users extends Component {
  render() {
    const { match } = this.props;
    return (
      <Switch>
        <Route exact path={match.url} component={UsersList} />
        <Route path={`${match.url}/edit/:id?`} component={UsersEdit} />
        <Route path={`${match.url}/:id`} component={UsersSingle} />
      </Switch>
    );
  }
}

export default withRouter(Users);
