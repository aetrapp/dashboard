import _ from "underscore";
import axios from "axios";
import { setWith, clone } from "lodash";
import React, { Component } from "react";
import client, { apiUrl } from "services/feathers";
import { addNotification } from "actions/notifications";
import { connect } from "react-redux";
import { Redirect } from "react-router-dom";
import {
  Segment,
  Header,
  Form,
  Select,
  Dropdown,
  Input,
  TextArea,
  Checkbox,
  Radio,
  Button,
  Icon
} from "semantic-ui-react";
import Datetime from "react-datetime";

class NewBroadcast extends Component {
  constructor(props) {
    super(props);
    this.service = client.service("broadcasts");
    this.usersService = client.service("users");
    this.citiesService = client.service("cities");
    this.state = {
      loading: false,
      done: false,
      citiesOptions: {},
      usersOptions: {},
      user: {},
      customDelivery: false,
      broadcast: {
        recipients: {
          cities: [],
          users: []
        },
        deliveryTime: new Date()
      }
    };
    this._onSubmit = this._onSubmit.bind(this);
    this._getUserAllowedCities = this._getUserAllowedCities.bind(this);
    this._searchCities = this._searchCities.bind(this);
    this._searchUsers = this._searchUsers.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this._handleCheckbox = this._handleCheckbox.bind(this);
    this._handleCustomDeliveryCheckbox = this._handleCustomDeliveryCheckbox.bind(
      this
    );
    this._handleDatetimeChange = this._handleDatetimeChange.bind(this);
  }
  componentDidMount() {
    const { auth } = this.props;
    if (auth && auth.user.id) {
      this.setState({ loading: true });
      this.usersService
        .get(auth.user.id)
        .then(user => {
          let type;
          if (user.roles.indexOf("admin") === -1) {
            type = "city";
          }
          this.setState({
            user,
            loading: false,
            broadcast: {
              type
            }
          });
          this._getUserAllowedCities();
        })
        .catch(err => {
          console.warn(err);
        });
    }
  }
  _searchCities = _.debounce((ev, data) => {
    const { user } = this.state;
    if (user.roles && user.roles.indexOf("admin") === -1) {
      return false;
    }
    if (data.searchQuery) {
      this.citiesService
        .find({
          query: {
            name: { $iLike: `%${data.searchQuery}%` }
          }
        })
        .then(res => {
          const cities = {};
          res.data.forEach(city => {
            cities[city.id] = {
              key: city.id,
              value: city.id,
              text: `${city.name} - ${city.stateId}`
            };
          });
          this.setState({
            citiesOptions: {
              ...this.state.citiesOptions,
              ...cities
            }
          });
        });
    }
  }, 200);
  _searchUsers = _.debounce((ev, data) => {
    const { user } = this.state;
    if (user.roles && user.roles.indexOf("admin") === -1) {
      return false;
    }
    if (data.searchQuery) {
      this.usersService
        .find({
          query: {
            $or: {
              firstName: { $iLike: `%${data.searchQuery}%` },
              lastName: { $iLike: `%${data.searchQuery}%` },
              email: { $iLike: `%${data.searchQuery}%` }
            }
          }
        })
        .then(res => {
          const users = {};
          res.data.forEach(user => {
            users[user.id] = {
              key: user.id,
              value: user.id,
              text: `${user.firstName} ${user.lastName} <${user.email}>`
            };
          });
          this.setState({
            usersOptions: {
              ...this.state.usersOptions,
              ...users
            }
          });
        });
    }
  }, 200);
  _handleChange = (e, { name, value }) => {
    let newBroadcast = Object.assign({}, this.state.broadcast);
    this.setState({
      broadcast: setWith(clone(newBroadcast), name, value, clone)
    });
    // this.setState({
    //   broadcast: {
    //     ...this.state.broadcast,
    //     [name]: value
    //   }
    // });
  };
  _handleDatetimeChange = date => {
    let deliveryTime;
    if (typeof date === "string") {
      deliveryTime = new Date();
    } else {
      deliveryTime = date.toDate();
    }
    this.setState({
      broadcast: {
        ...this.state.broadcast,
        deliveryTime
      }
    });
  };
  _handleCheckbox = (e, { name, value }) => {
    const checked = this.state.user[name];
    this.setState({
      broadcast: {
        ...this.state.broadcast,
        [name]: !checked
      }
    });
  };
  _handleCustomDeliveryCheckbox = (e, { name, value }) => {
    const checked = this.state.customDelivery;
    const { deliveryTime } = this.state.broadcast;
    this.setState({
      customDelivery: !checked,
      broadcast: {
        ...this.state.broadcast,
        deliveryTime: deliveryTime || new Date()
      }
    });
  };
  _onSubmit() {
    this.setState({ loading: true });
    const { addNotification } = this.props;
    let { user, customDelivery } = this.state;
    let broadcast = Object.assign({}, this.state.broadcast);
    let errored = false;
    switch (broadcast.type) {
      case "city":
        const singleCity = this._isSingleCityMod();
        if (singleCity) {
          broadcast.recipients.cities = [singleCity.value];
        } else if (
          !broadcast.recipients ||
          !broadcast.recipients.cities ||
          !broadcast.recipients.cities.length
        ) {
          errored = true;
          addNotification({
            message: "Erro: Você deve selecionar ao menos uma localização",
            level: "error"
          });
        }
        break;
      case "direct":
        if (
          !broadcast.recipients ||
          !broadcast.recipients.users ||
          !broadcast.recipients.users.length
        ) {
          errored = true;
          addNotification({
            message: "Erro: Você deve selecionar ao menos um usuário",
            level: "error"
          });
        }
    }
    if (!customDelivery) {
      delete broadcast.deliveryTime;
    }
    if (!errored) {
      this.service
        .create(broadcast)
        .then(data => {
          this.setState({ loading: false });
          addNotification({
            message: "Mensagem salva com sucesso",
            level: "success"
          });
          this.setState({
            broadcast: {
              recipients: {
                cities: []
              },
              deliveryTime: new Date(),
              type: this.state.broadcast.type
            }
          });
        })
        .catch(err => {
          this.setState({ loading: false });
          addNotification({
            message: "Erro: " + err.message,
            level: "error"
          });
        });
    } else {
      this.setState({ loading: false });
    }
  }
  _getAllowedCitiesPath(user) {
    return `${apiUrl}/users/${user.id}/allowedCities`;
  }
  _getUserAllowedCities() {
    const { user } = this.state;
    if (user.id && user.roles && user.roles.indexOf("admin") === -1) {
      const path = this._getAllowedCitiesPath(user);
      axios.get(path).then(res => {
        const cities = {};
        res.data.forEach(city => {
          cities[city.id] = {
            key: city.id,
            value: city.id,
            text: `${city.name} - ${city.stateId}`
          };
        });
        this.setState({
          citiesOptions: {
            ...this.state.citiesOptions,
            ...cities
          },
          user: {
            ...this.state.user,
            allowedCities: res.data.map(city => city.id)
          }
        });
      });
    }
  }
  _isSingleCityMod() {
    const { user, citiesOptions } = this.state;
    if (user.roles && user.roles.indexOf("admin") !== -1) {
      return false;
    }
    if (
      user.roles &&
      user.roles.indexOf("moderator") !== -1 &&
      user.allowedCities &&
      user.allowedCities.length == 1
    ) {
      return citiesOptions[user.allowedCities[0]];
    } else {
      return false;
    }
  }
  render() {
    const { auth } = this.props;
    const {
      loading,
      done,
      broadcast,
      customDelivery,
      user,
      citiesOptions,
      usersOptions
    } = this.state;
    const singleCity = this._isSingleCityMod();
    return (
      <Segment>
        <Header as="h3">Broadcast</Header>
        <Form onSubmit={this._onSubmit}>
          {!broadcast.type ? (
            <Header as="h4">Selecione o alvo da mensagem</Header>
          ) : null}
          {user.roles && user.roles.indexOf("admin") !== -1 ? (
            <Form.Group>
              <Form.Field
                control={Radio}
                name="type"
                value="global"
                label="Todos os usuários"
                checked={broadcast.type == "global"}
                onChange={this._handleChange}
              />
              <Form.Field
                control={Radio}
                name="type"
                value="city"
                label="Usuários com armadilhas em cidades específicas"
                checked={broadcast.type == "city"}
                onChange={this._handleChange}
              />
              <Form.Field
                control={Radio}
                name="type"
                value="direct"
                label="Usuário específico"
                checked={broadcast.type == "direct"}
                onChange={this._handleChange}
              />
            </Form.Group>
          ) : null}
          {broadcast.type ? (
            <div>
              <Form.Field
                control={Input}
                label="Título"
                placeholder="Título da mensagem"
                name="title"
                onChange={this._handleChange}
              />
              <Form.Field
                control={TextArea}
                rows="5"
                label="Mensagem"
                placeholder="Digite a mensagem a ser enviada"
                name="message"
                onChange={this._handleChange}
              />
              {broadcast.type == "city" ? (
                <Form.Field>
                  {singleCity ? (
                    <p>
                      Mensagem será enviada à <strong>{singleCity.text}</strong>.
                    </p>
                  ) : (
                    <Form.Field
                      control={Dropdown}
                      fluid
                      search
                      selection
                      multiple
                      label="Selecione os locais para enviar a mensagem"
                      name="recipients.cities"
                      placeholder="Busque cidades..."
                      onChange={this._handleChange}
                      onSearchChange={this._searchCities}
                      options={Object.values(citiesOptions)}
                    />
                  )}
                </Form.Field>
              ) : null}
              {broadcast.type == "direct" ? (
                <Form.Field
                  control={Dropdown}
                  fluid
                  search
                  selection
                  multiple
                  label="Selecione usuários que receberão a mensagem (busca por email)"
                  name="recipients.users"
                  placeholder="Busque usuários..."
                  onChange={this._handleChange}
                  onSearchChange={this._searchUsers}
                  options={Object.values(usersOptions)}
                />
              ) : null}
              <Form.Field
                control={Checkbox}
                label={{
                  children:
                    "Definir horário customizado de envio (caso contrário será enviado imediatamente)"
                }}
                onChange={this._handleCustomDeliveryCheckbox}
              />
              {customDelivery ? (
                <Form.Field
                  as={Datetime}
                  label="Selecione a data para envio"
                  locale="pt-br"
                  name="deliveryTime"
                  value={broadcast.deliveryTime}
                  onChange={this._handleDatetimeChange}
                />
              ) : null}
              <Button fluid primary icon type="submit" disabled={loading}>
                <Icon name={loading ? "spinner" : "save"} />{" "}
                {loading ? (
                  <span>Enviando...</span>
                ) : (
                  <span>Enviar mensagem</span>
                )}
              </Button>
            </div>
          ) : null}
        </Form>
        {!auth.signedIn ? <Redirect to="/" /> : null}
      </Segment>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = {
  addNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(NewBroadcast);
