export const NOTIFICATION_ADD = "NOTIFICATION_ADD";

export const addNotification = notification => dispatch => {
  return dispatch({
    type: NOTIFICATION_ADD,
    notification
  });
};
