import React from 'react';
import ReactDom from 'react-dom';
import { Provider } from 'react-redux';
import createHistory from 'history/createBrowserHistory'
import { ConnectedRouter as Router } from 'react-router-redux';

import configureStore from 'store';
import Application from 'app';

import authService from 'services/auth';

const store = configureStore();
const history = createHistory();

authService(store);

ReactDom.render(
  <Provider store={store}>
    <Router history={history}>
      <Application />
    </Router>
  </Provider>,
  document.getElementById('app')
);
