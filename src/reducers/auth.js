import {
  AUTH_REQUEST,
  AUTH_SUCCESS,
  AUTH_FAILURE,
  AUTH_LOGOUT_SUCCESS
} from "actions/auth";

const initialState = {
  error: false,
  signedIn: false,
  loading: false,
  user: undefined
};

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case AUTH_REQUEST: {
      return {
        ...initialState,
        ...state,
        error: false,
        loading: true
      };
    }
    case AUTH_SUCCESS: {
      return {
        ...initialState,
        ...state,
        loading: false,
        signedIn: true,
        user: {
          ...action.user
        }
      };
    }
    case AUTH_FAILURE: {
      return {
        ...initialState,
        ...state,
        loading: false,
        signedIn: false,
        error: action.err.message
      };
    }
    case AUTH_LOGOUT_SUCCESS: {
      return {
        ...state,
        ...initialState
      };
    }
    default:
      return state;
  }
}
