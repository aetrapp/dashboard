import { combineReducers } from "redux";
import auth from "./auth";
import notifications from "./notifications";
import { routerReducer } from "react-router-redux";

export default function() {
  return combineReducers({
    auth,
    notifications,
    router: routerReducer
  });
}
