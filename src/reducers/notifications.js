import { NOTIFICATION_ADD } from "actions/notifications";

const initialState = null;

export default function reducer(state = initialState, action) {
  switch (action.type) {
    case NOTIFICATION_ADD: {
      return action.notification;
    }
    default:
      return state;
  }
}
