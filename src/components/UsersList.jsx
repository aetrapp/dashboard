import React from "react";
import _ from "underscore";
import moment from "moment";
import { connect } from "react-redux";
import { addNotification } from "actions/notifications";
import {
  Header,
  Segment,
  Form,
  Input,
  Select,
  Table,
  Label,
  Button,
  Icon,
  Divider,
  Dimmer,
  Loader
} from "semantic-ui-react";
import { Link, Redirect } from "react-router-dom";
import UsersNav from "components/UsersNav.jsx";
import client from "services/feathers";
import DownloadButtons from "components/DownloadButtons.jsx";
import FilteredDownload from "components/FilteredDownload.jsx";
import SortedHeaderCell from "components/SortedHeaderCell.jsx";

class UsersList extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      clicked: {},
      users: [],
      query: { $limit: 10, $sort: {} },
      filter: {},
      total: 0,
      hasMore: false,
      page: 1
    };
    this._loadMore = this._loadMore.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this._updateUsers = this._updateUsers.bind(this);
    this._toggleActive = this._toggleActive.bind(this);
    this._remove = this._remove.bind(this);
  }
  componentDidMount() {
    this._updateUsers();
  }
  componentDidUpdate(prevProps, prevState) {
    const { query } = this.state;
    if (JSON.stringify(query) !== JSON.stringify(prevState.query)) {
      this._updateUsers();
    }
  }
  _updateUsers() {
    const { query } = this.state;
    this.setState({ loading: true });
    let sendQuery = { ...query };
    if (sendQuery.$sort) {
      const sortKey = Object.keys(sendQuery.$sort)[0];
      if (sortKey && !sendQuery[sortKey]) sendQuery[sortKey] = { $ne: null };
    }
    client
      .service("users")
      .find({ query: sendQuery })
      .then(res => {
        this.setState({
          loading: false,
          total: res.total,
          page: 1,
          users: res.data
        });
        this._updatePaging(res, 1);
      })
      .catch(err => {
        this.setState({ loading: false });
        console.log(err);
      });
  }
  _updatePaging(res, page = false) {
    if (!page) page = this.state.page;
    if (res.limit * page < res.total) {
      this.setState({
        hasMore: true
      });
    } else {
      this.setState({
        hasMore: false
      });
    }
  }
  _loadMore() {
    const { query, page } = this.state;
    client
      .service("users")
      .find({
        query: Object.assign({}, query, {
          $skip: page * query.$limit
        })
      })
      .then(res => {
        this.setState({ page: page + 1 });
        this.setState({
          users: [...this.state.users, ...res.data]
        });
        this._updatePaging(res, page + 1);
      });
  }
  _handleChange = (e, { name, value }) => {
    const { query } = this.state;
    let newQuery = Object.assign({}, query);
    delete newQuery["$or"];
    delete newQuery["roles"];
    delete newQuery["trapCount"];
    delete newQuery["delayedTrapCount"];
    switch (name) {
      case "q":
        newQuery["$or"] = {
          firstName: { $iLike: `%${value}%` },
          lastName: { $iLike: `%${value}%` },
          id: { $iLike: `%${value}%` },
          email: { $iLike: `%${value}%` }
        };
        break;
      case "role":
        if (value) newQuery["roles"] = { $contains: [value] };
        break;
      case "trapFilter":
        if (value == "withTrap") {
          newQuery["trapCount"] = { $gte: 1 };
        } else if (value == "withDelayedTrap") {
          newQuery["delayedTrapCount"] = { $gte: 1 };
        }
        break;
    }
    this.setState({
      query: newQuery
    });
  };
  _handleSortClick = key => ev => {
    const { clicked } = this.state;
    const keyClicked = (clicked[key] || 0) + 1;
    const reset = keyClicked % 3 == 0;
    if (!reset) {
      this._handleSort(key)();
    } else {
      this._handleSort("")();
    }
    this.setState({
      clicked: {
        ...clicked,
        [key]: reset ? 0 : keyClicked
      }
    });
  };
  _handleSort = field => () => {
    const { query } = this.state;
    const current = Object.keys(query.$sort)[0];
    const $sort = field
      ? {
          [field]: current == field ? -query.$sort[field] : 1
        }
      : {};
    this.setState({
      query: {
        ...query,
        $sort
      }
    });
  };
  _getSort = field => {
    const { query } = this.state;
    const current = Object.keys(query.$sort)[0];
    if (field == current) {
      if (query.$sort[current] == 1) {
        return "descending";
      } else {
        return "ascending";
      }
    }
    return null;
  };
  _handleEditUser = userId => () => {
    this.setState({ go: `/users/edit/${userId}` });
  };
  _toggleActive(userId, isActive) {
    const { addNotification } = this.props;
    return () => {
      client
        .service("users")
        .patch(userId, { isActive })
        .then(() => {
          let users = this.state.users.slice(0);
          users = users.map(user => {
            if (user.id == userId) {
              user.isActive = isActive;
            }
            return user;
          });
          this.setState({ users });
          addNotification({
            message: "Usuário modificado com sucesso",
            level: "success"
          });
        })
        .catch(err => {
          addNotification({
            message: "Erro: " + err.message,
            level: "error"
          });
        });
    };
  }
  _remove(userId) {
    const { addNotification } = this.props;
    return () => {
      if (
        confirm(
          "Você tem certeza que deseja remover? O usuário será removido permanentemente do banco de dados."
        )
      ) {
        client
          .service("users")
          .remove(userId)
          .then(() => {
            let users = this.state.users.slice(0);
            users = users.filter(user => {
              return user.id !== userId;
            });
            this.setState({ users });
            addNotification({
              message: "Usuário removido com sucesso",
              level: "success"
            });
          })
          .catch(err => {
            addNotification({
              message: "Erro: " + err.message,
              level: "error"
            });
          });
      }
    };
  }
  _hasFilter() {
    const query = { ...this.state.query };
    delete query.$limit;
    delete query.$sort;
    if (Object.keys(query).length) {
      return true;
    }
    return false;
  }
  _filteredDownloadUrl() {
    const query = { ...this.state.query };
    delete query.$limit;
    delete query.$sort;
    query.format = "csv";
    return restUrl("users", query);
  }
  render() {
    const { loading, go, users, total, query, hasMore } = this.state;
    return (
      <div>
        <Form onSubmit={this._doSearch}>
          <Form.Group>
            <Form.Field
              width="10"
              control={Input}
              onChange={this._handleChange}
              name="q"
              placeholder="Buscar por ID, nome ou email"
            />
            <Form.Field
              width="4"
              control={Select}
              onChange={this._handleChange}
              fluid
              name="role"
              placeholder="Filtrar por função"
              options={[
                {
                  key: "",
                  value: "",
                  text: "Todas"
                },
                {
                  key: "admin",
                  value: "admin",
                  text: "Admin"
                },
                {
                  key: "moderator",
                  value: "moderator",
                  text: "Moderador"
                }
              ]}
            />
            <Form.Field
              width="4"
              control={Select}
              onChange={this._handleChange}
              fluid
              name="trapFilter"
              placeholder="Filtrar por armadilhas"
              options={[
                {
                  key: "",
                  value: "",
                  text: "Todos"
                },
                {
                  key: "withTrap",
                  value: "withTrap",
                  text: "Com armadilhas"
                },
                {
                  key: "withDelayedTrap",
                  value: "withDelayedTrap",
                  text: "Com armadilhas atrasadas"
                }
              ]}
            />
          </Form.Group>
        </Form>
        <UsersNav />
        <DownloadButtons />
        <FilteredDownload
          service="users"
          query={query}
          basic
          size="tiny"
          icon
          floated="right"
          style={{ marginRight: "1rem" }}
        >
          <Icon name="download" /> Baixar resultados filtrados
        </FilteredDownload>
        <Button
          floated="right"
          basic
          size="tiny"
          icon
          onClick={this._updateUsers}
          style={{ marginRight: "1rem" }}
        >
          <Icon name="refresh" loading={loading} /> Atualizar resultados
        </Button>
        <p
          style={{
            float: "left",
            lineHeight: "30px",
            margin: "0 0 0 1rem"
          }}
        >
          {total} usuários encontrados.
        </p>
        <Divider hidden clearing />
        <div style={{ position: "relative", minHeight: "300px" }}>
          <Dimmer active={loading} inverted>
            <Loader />
          </Dimmer>
          {users.length ? (
            <Table sortable>
              <Table.Header>
                <Table.Row>
                  <SortedHeaderCell>ID</SortedHeaderCell>
                  <SortedHeaderCell
                    onClick={this._handleSortClick("firstName")}
                    sorted={this._getSort("firstName")}
                  >
                    Nome
                  </SortedHeaderCell>
                  <SortedHeaderCell
                    onClick={this._handleSortClick("trapCount")}
                    sorted={this._getSort("trapCount")}
                  >
                    Nº armadilhas
                  </SortedHeaderCell>
                  <SortedHeaderCell>Telefone</SortedHeaderCell>
                  <SortedHeaderCell>Celular</SortedHeaderCell>
                  <SortedHeaderCell>Email</SortedHeaderCell>
                  <SortedHeaderCell
                    onClick={this._handleSortClick("createdAt")}
                    sorted={this._getSort("createdAt")}
                  >
                    Registro
                  </SortedHeaderCell>
                  <SortedHeaderCell>Função</SortedHeaderCell>
                  <SortedHeaderCell>Ações</SortedHeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {users.map(user => (
                  <Table.Row key={user.id}>
                    <Table.Cell>{user.id}</Table.Cell>
                    <Table.Cell>
                      <Link to={`/users/${user.id}`}>
                        {user.firstName} {user.lastName}
                      </Link>
                    </Table.Cell>
                    <Table.Cell>
                      {user.trapCount}{" "}
                      {user.delayedTrapCount ? (
                        <Label>{user.delayedTrapCount} atrasada(s)</Label>
                      ) : null}
                    </Table.Cell>
                    <Table.Cell>{user.landlineNumber}</Table.Cell>
                    <Table.Cell>{user.cellphoneNumber}</Table.Cell>
                    <Table.Cell>{user.email}</Table.Cell>
                    <Table.Cell>
                      {moment(user.createdAt).format("L")}
                    </Table.Cell>
                    {user.roles && user.roles.length ? (
                      <Table.Cell>
                        {user.roles.map(role => (
                          <Label key={role} size="small">
                            {role}
                          </Label>
                        ))}
                      </Table.Cell>
                    ) : (
                      <Table.Cell>
                        <Label>regular</Label>
                      </Table.Cell>
                    )}
                    <Table.Cell>
                      <Button.Group widths="3" attached size="tiny" vertical>
                        <Button
                          icon
                          basic
                          color="grey"
                          onClick={this._handleEditUser(user.id)}
                        >
                          <Icon name="edit" /> Editar
                        </Button>
                        <Button
                          icon
                          basic
                          color={user.isActive ? "yellow" : "green"}
                          onClick={this._toggleActive(user.id, !user.isActive)}
                        >
                          {user.isActive ? (
                            <span>
                              <Icon name="warning sign" /> Desativar
                            </span>
                          ) : (
                            <span>
                              <Icon name="warning sign" /> Ativar
                            </span>
                          )}
                        </Button>
                        <Button
                          icon
                          basic
                          color="red"
                          onClick={this._remove(user.id)}
                        >
                          <Icon name="trash" /> Remover
                        </Button>
                      </Button.Group>
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          ) : (
            <Header as="h4">Nenhum usuário foi encontrado</Header>
          )}
        </div>
        {hasMore ? (
          <Button fluid basic onClick={this._loadMore}>
            Carregar mais usuários
          </Button>
        ) : null}
        {go ? <Redirect to={go} push /> : null}
      </div>
    );
  }
}

const mapDispatchToProps = {
  addNotification
};

export default connect(null, mapDispatchToProps)(UsersList);
