import React from "react";
import { Icon } from "semantic-ui-react";
import { getStatusColor } from "lib/traps";

export default class TrapStatus extends React.Component {
  render() {
    const { status } = this.props;
    return (
      <Icon
        name="circle"
        style={{
          color: getStatusColor(status)
        }}
      />
    );
  }
}
