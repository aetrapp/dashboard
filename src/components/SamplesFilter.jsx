import _ from "underscore";
import React from "react";
import client from "services/feathers";
import { Form, Input, Select, Dropdown } from "semantic-ui-react";

export default class SamplesFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      values: {}
    };
    this._handleChange = this._handleChange.bind(this);
  }
  componentDidUpdate(prevProps, prevState) {
    const { onChange } = this.props;
    const { values } = this.state;
    if (
      onChange &&
      JSON.stringify(prevState.values) !== JSON.stringify(values)
    ) {
      onChange(values);
    }
  }
  _handleChange(e, { name, value }) {
    this.setState({
      values: {
        ...this.state.values,
        [name]: value
      }
    });
  }
  render() {
    return (
      <Form>
        <Form.Group widths="equal">
          <Form.Field
            width="11"
            control={Input}
            placeholder="Buscar por mensagem de erro ou ID"
            name="error"
            onChange={this._handleChange}
          />
          <Form.Field
            width="5"
            control={Select}
            placeholder="Filtrar por status"
            onChange={this._handleChange}
            name="status"
            options={[
              {
                key: "",
                value: "",
                text: "Todas"
              },
              {
                key: "unprocessed",
                value: "unprocessed",
                text: "Não processadas"
              },
              {
                key: "valid",
                value: "valid",
                text: "Válidas"
              },
              {
                key: "invalid",
                value: "invalid",
                text: "Inválidas"
              }
            ]}
          />
        </Form.Group>
      </Form>
    );
  }
}
