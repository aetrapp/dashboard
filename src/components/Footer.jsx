import React from "react";
import { Container, Grid } from "semantic-ui-react";

export default props => {
  return (
    <section
      id="sp-bottom-wrapper"
      className=" "
      style={{
        background: "#2d343e",
        padding: "40px 0",
        color: "#cccccc"
      }}
    >
      <Container>
        <Grid columns={4}>
          <Grid.Row>
            <Grid.Column>
              <div className="jm-module module ">
                <div className="mod-wrapper-flat clearfix">
                  <div className="custom">
                    <p>
                      <img alt="" src={require("images/logo.png")} />
                    </p>
                    <p>
                      Monitoramento cidadão de focos de mosquitos <em>Aedes</em>,
                      transmissores de dengue, zika, chikungunya e febre amarela
                      urbana. Comunidades gerando dados para o combate aos
                      mosquitos.<br />
                      <br />
                    </p>
                  </div>
                </div>
              </div>
            </Grid.Column>

            <Grid.Column>
              <div>
                <div id="text-5">
                  <h5>NAVEGAÇÃO RÁPIDA</h5>{" "}
                  <div>
                    <p>
                      <a href="http://painel.aetrapp.org/">Mapa</a>
                    </p>
                  </div>
                </div>
                <div id="text-4">
                  <div>
                    <p>
                      <a href="https://www.aetrapp.org/caderno-pedagogico/">
                        Caderno Pedagógico
                      </a>
                    </p>
                  </div>
                </div>
                <div id="text-6">
                  <div>
                    <p>
                      <a href="https://www.aetrapp.org/como-construir-uma-armadilha/">
                        Como construir uma armadilha
                      </a>
                    </p>
                  </div>
                </div>
                <div id="text-7">
                  <div>
                    <p>
                      <a href="https://www.aetrapp.org/como-fotografar-uma-amostra/">
                        Como fotografar uma amostra
                      </a>
                    </p>
                  </div>
                </div>
                <div id="text-8">
                  <div>
                    <p>
                      <a href="https://www.aetrapp.org/videos/">Vídeos</a>
                    </p>
                  </div>
                </div>
              </div>
            </Grid.Column>

            <Grid.Column>
              <div id="sp-bottom2" className="span4">
                {" "}
                <div className="jm-module module ">
                  <div className="mod-wrapper-flat clearfix">
                    <h3 className="header">
                      <span className="mod-title">
                        <span className="color">Licença </span>{" "}
                      </span>
                    </h3>

                    <div className="custom">
                      <p>
                        <a
                          href="https://creativecommons.org/licenses/by-sa/4.0/deed.pt_BR"
                          target="_blank"
                          rel="license"
                        >
                          <img
                            alt="Creative Commons License"
                            src="https://licensebuttons.net/l/by-sa/4.0/88x31.png"
                          />
                        </a>
                        <br />Creative Commons<br />Atribuição-CompartilhaIgual<br />4.0
                        International<br />(CC BY-SA 4.0)
                      </p>
                    </div>
                  </div>
                </div>
                <div className="gap" />
              </div>
            </Grid.Column>

            <Grid.Column>
              <div id="sp-bottom3" className="span4">
                {" "}
                <div className="jm-module module ">
                  <div className="mod-wrapper-flat clearfix">
                    <h3 className="header">
                      <span className="mod-title">
                        <span className="color">Realização </span>{" "}
                      </span>
                    </h3>

                    <div className="custom">
                      <p>
                        <a
                          href="https://www.aetrapp.org/equipe"
                          target="_blank"
                        >
                          <img
                            alt="Logomarca dos Parceiros Realizadores"
                            src="https://www.aetrapp.org/wp-content/uploads/2020/05/realizacao_parceiros_rodape_1024x304.png"
                          />
                        </a>
                      </p>
                    </div>
                  </div>
                </div>
                <div className="gap" />
              </div>
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Container>
    </section>
  );
};
