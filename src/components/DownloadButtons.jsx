import React from "react";
import axios from "axios";
import FileSaver from "file-saver";
import { connect } from "react-redux";
import { addNotification } from "actions/notifications";
import { apiUrl } from "services/feathers";
import { Button, Icon } from "semantic-ui-react";
import Restrict from "./Restrict.jsx";

class DownloadButtons extends React.Component {
  _handleExport = service => ev => {
    ev.preventDefault();
    const { addNotification } = this.props;
    const BOM = "\uFEFF";
    let req;

    req = axios.get(`${apiUrl}/downloads/${service}.csv`, {
      headers: {
        Authorization: window.localStorage["feathers-jwt"]
      }
    });

    req
      .then(res => {
        const blob = new Blob([BOM + res.data], {
          type: "text/csv;charset=utf-8"
        });
        FileSaver.saveAs(blob, "users.csv");
      })
      .catch(err => {
        addNotification({
          message: "Erro: " + err.response.statusText || err.message,
          level: "error"
        });
      });
  };
  render() {
    const { children } = this.props;
    return (
      <Button.Group size="tiny" basic floated="right">
        {children}
        <Restrict roles={["admin", "moderator"]}>
          <Button
            icon
            onClick={this._handleExport("users")}
            href={`${apiUrl}/downloads/users.csv`}
          >
            <Icon name="download" /> Baixar usuários
          </Button>
        </Restrict>
        <Button icon href={`${apiUrl}/downloads/traps.csv`}>
          <Icon name="download" /> Baixar armadilhas
        </Button>
        <Button icon href={`${apiUrl}/downloads/samples.csv`}>
          <Icon name="download" /> Baixar amostras
        </Button>
      </Button.Group>
    );
  }
}

const mapDispatchToProps = {
  addNotification
};

export default connect(null, mapDispatchToProps)(DownloadButtons);
