import React, { Component } from "react";
import { connect } from "react-redux";
import { Segment, Header, Form, Button, Divider } from "semantic-ui-react";
import { Redirect } from "react-router-dom";
import { authenticate } from "actions/auth";
import { addNotification } from "actions/notifications";
import client from "services/feathers";

class SignIn extends Component {
  _notificationSystem = null;
  constructor(props) {
    super(props);
    this.state = {
      action: "signin",
      email: "",
      password: ""
    };
    this._onSubmit = this._onSubmit.bind(this);
    this._changeAction = this._changeAction.bind(this);
    this._handleChange = this._handleChange.bind(this);
    this.authManagement = client.service("authManagement");
  }
  componentDidMount() {
    const { match, addNotification } = this.props;
    if (match.params.verifyToken) {
      this.authManagement
        .create({
          action: "verifySignupLong",
          value: match.params.verifyToken
        })
        .then(() => {
          addNotification({
            message: "Email verificado com sucesso.",
            level: "success"
          });
        })
        .catch(err => {
          addNotification({
            message: "Erro: " + err.message,
            level: "error"
          });
        });
    }

    if (match.params.resetToken) {
      this.setState({
        action: "resetPassword"
      });
    }
  }
  componentWillReceiveProps(nextProps) {
    const { error, addNotification } = this.props;
    if (nextProps.error && nextProps.error !== error) {
      addNotification({
        message: "Erro: " + nextProps.error,
        level: "error"
      });
    }
  }
  _changeAction(action) {
    return () => {
      this.setState({ action });
    };
  }
  _buttonLabel() {
    const { action } = this.state;
    switch (action) {
      case "signin":
        return "Entrar";
      case "forgotPassword":
        return "Recuperar senha";
      case "resetPassword":
        return "Alterar senha";
      case "resendVerification":
        return "Reenviar email de confirmação";
    }
  }
  _handleChange = (e, { name, value }) => this.setState({ [name]: value });
  _onSubmit(e) {
    e.preventDefault();
    const { addNotification, match } = this.props;
    const { action, email, password, password_confirm } = this.state;
    switch (action) {
      case "signin":
        this.props.authenticate({ email, password });
        break;
      case "forgotPassword":
        this.authManagement
          .create({
            action: "sendResetPwd",
            value: {
              email
            }
          })
          .then(() => {
            addNotification({
              message: "Verifique seu email para confirmar seu usuário.",
              level: "success"
            });
          })
          .catch(err => {
            addNotification({
              message: "Erro: " + err.message,
              level: "error"
            });
          });
        break;
      case "resendVerification":
        this.authManagement
          .create({
            action: "resendVerifySignup",
            value: {
              email
            }
          })
          .then(() => {
            addNotification({
              message:
                "Verifique sua caixa de email para confirmar o endereço.",
              level: "success"
            });
          })
          .catch(err => {
            addNotification({
              message: "Erro: " + err.message,
              level: "error"
            });
          });
        break;
      case "resetPassword":
        if (!password) {
          addNotification({
            message: "Erro: Você deve digitar uma nova senha",
            level: "error"
          });
        } else if (password !== password_confirm) {
          addNotification({
            message: "Erro: Verifique se as senhas digitadas são iguais.",
            level: "error"
          });
        } else {
          this.authManagement
            .create({
              action: "resetPwdLong",
              value: {
                token: match.params.resetToken,
                password
              }
            })
            .then(() => {
              addNotification({
                message: "Senha alterada com sucesso.",
                level: "success"
              });
              this.setState({
                action: "signin"
              });
            })
            .catch(err => {
              addNotification({
                message: "Erro: " + err.message,
                level: "error"
              });
            });
        }
    }
  }
  render() {
    const { action } = this.state;
    const { signedIn, match } = this.props;
    if (signedIn) {
      return <Redirect to="/" />;
    } else {
      return (
        <Segment raised padded>
          <Header as="h4" color="grey">
            {this._buttonLabel()}
          </Header>
          <Form onSubmit={this._onSubmit}>
            {action !== "resetPassword" ? (
              <Form.Input
                icon="user"
                iconPosition="left"
                type="email"
                name="email"
                onChange={this._handleChange}
                value={this.state.email}
                placeholder="Email"
              />
            ) : null}
            {action == "signin" || action == "resetPassword" ? (
              <Form.Input
                icon="lock"
                iconPosition="left"
                type="password"
                name="password"
                onChange={this._handleChange}
                value={this.state.password}
                placeholder="Senha"
              />
            ) : null}
            {action == "resetPassword" ? (
              <Form.Input
                icon="lock"
                iconPosition="left"
                type="password"
                name="password_confirm"
                onChange={this._handleChange}
                value={this.state.password_confirm}
                placeholder="Confirme sua nova senha"
              />
            ) : null}
            <Button fluid primary type="submit">
              {this._buttonLabel()}
            </Button>
          </Form>
          <Divider />
          {action !== "resetPassword" && !match.params.verifyToken ? (
            <Button.Group fluid basic>
              {action !== "signin" ? (
                <Button onClick={this._changeAction("signin")}>
                  Fazer login
                </Button>
              ) : null}
              {action !== "forgotPassword" ? (
                <Button onClick={this._changeAction("forgotPassword")}>
                  Esqueci minha senha
                </Button>
              ) : null}
              {action !== "resendVerification" ? (
                <Button onClick={this._changeAction("resendVerification")}>
                  Reenviar email de confirmação
                </Button>
              ) : null}
            </Button.Group>
          ) : null}
        </Segment>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    signedIn: state.auth.signedIn,
    error: state.auth.error
  };
};

const mapDispatchToProps = {
  authenticate,
  addNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(SignIn);
