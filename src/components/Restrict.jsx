import React from "react";
import { connect } from "react-redux";

class Restrict extends React.Component {
  _isInRoles(user, roles) {
    if (Array.isArray(roles)) {
      let inRole = false;
      roles.forEach(role => {
        if (user.roles.indexOf(role) !== -1) {
          inRole = true;
        }
      });
      return inRole;
    } else {
      return user.roles.indexOf(roles) !== -1;
    }
  }
  _hasCity = () => {
    const { auth, cityRestrict } = this.props;
    if (
      auth.user.cities &&
      auth.user.cities.length &&
      auth.user.cities.indexOf(cityRestrict) !== -1
    )
      return true;
    return false;
  };
  render() {
    const { auth, roles, cityRestrict, children } = this.props;
    if (
      auth.signedIn &&
      auth.user &&
      auth.user.roles.length &&
      this._isInRoles(auth.user, roles) &&
      (cityRestrict && auth.user.roles.indexOf("admin") == -1
        ? this._hasCity()
        : true)
    ) {
      return React.Children.only(children);
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps)(Restrict);
