import _ from "underscore";
import React from "react";
import client from "services/feathers";
import { Form, Input, Select, Dropdown, Button, Icon } from "semantic-ui-react";
import { apiUrl } from "services/feathers";
import { statuses } from "lib/traps";

export default class TrapsFilter extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      citiesOptions: {},
      values: {}
    };
    this._handleChange = this._handleChange.bind(this);
  }
  componentDidUpdate(prevProps, prevState) {
    const { onChange } = this.props;
    const { values } = this.state;
    if (
      onChange &&
      JSON.stringify(prevState.values) !== JSON.stringify(values)
    ) {
      onChange(values);
    }
  }
  _handleChange(e, { name, value }) {
    this.setState({
      values: {
        ...this.state.values,
        [name]: value
      }
    });
  }
  _searchCities = _.debounce((ev, data) => {
    if (data.searchQuery) {
      client
        .service("cities")
        .find({
          query: {
            name: { $iLike: `%${data.searchQuery}%` }
          }
        })
        .then(res => {
          const cities = {};
          res.data.forEach(city => {
            cities[city.id] = {
              key: city.id,
              value: city.id,
              text: `${city.name} - ${city.stateId}`
            };
          });
          this.setState({
            citiesOptions: {
              ...this.state.citiesOptions,
              ...cities
            }
          });
        });
    }
  }, 200);
  render() {
    const { citiesOptions } = this.state;
    return (
      <Form
        style={{
          position: "relative",
          zIndex: "10000"
        }}
      >
        <Form.Group widths="equal">
          <Form.Field
            control={Input}
            placeholder="Buscar por ID ou endereço de rua"
            name="q"
            onChange={this._handleChange}
          />
          <Form.Field
            control={Dropdown}
            fluid
            search
            selection
            multiple
            placeholder="Busca por cidade"
            name="cities"
            onChange={this._handleChange}
            onSearchChange={this._searchCities}
            options={Object.values(citiesOptions)}
          />
          <Form.Field
            control={Select}
            placeholder="Filtrar por situação"
            onChange={this._handleChange}
            name="status"
            options={[
              {
                key: "",
                value: "",
                text: "Todas"
              },
              ...Object.keys(statuses).map(key => {
                return {
                  key: key,
                  value: key,
                  text: statuses[key]
                };
              })
            ]}
          />
        </Form.Group>
      </Form>
    );
  }
}
