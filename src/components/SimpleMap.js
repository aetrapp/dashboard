import React, { Component } from "react";
import styled from "styled-components";
import { Map, Marker, Popup, TileLayer } from "react-leaflet";
import { getStatusColor } from "lib/traps";

import L from "leaflet";
const imagePath = "/";
L.Icon.Default.imagePath = imagePath;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png").split(
    imagePath
  )[1],
  iconUrl: require("leaflet/dist/images/marker-icon.png").split(imagePath)[1],
  shadowUrl: require("leaflet/dist/images/marker-shadow.png").split(
    imagePath
  )[1]
});

const Wrapper = styled.div`
  .trap-icon {
    width: 50px;
    height: 50px;
    color: #fff;
    text-align: center;
    line-height: 30px;
  }
`;

class SimpleMap extends Component {
  trapToIcon() {
    const { trap } = this.props;
    return L.divIcon({
      html: `<div style="fill:${getStatusColor(
        trap.status
      )}">${require("images/map-marker-circle.svg")}</div>`,
      iconSize: [50, 50],
      iconAnchor: [25, 50],
      popupAnchor: [0, -50],
      className: `trap-icon ${trap.status}`
    });
  }
  render() {
    let { coordinates, height } = this.props;
    if (coordinates && coordinates.length) {
      return (
        <Wrapper>
          <Map
            center={coordinates.reverse()}
            zoom={15}
            scrollWheelZoom={false}
            style={{
              width: "100%",
              height: height || "600px"
            }}
          >
            <TileLayer
              url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
              attribution="&copy; <a href=&quot;https://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
            />
            <Marker icon={this.trapToIcon()} position={coordinates} />
          </Map>
        </Wrapper>
      );
    } else {
      return null;
    }
  }
}

export default SimpleMap;
