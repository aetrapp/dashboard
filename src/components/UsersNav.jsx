import React, { Component } from "react";
import { Link } from "react-router-dom";
import { Button, Icon } from "semantic-ui-react";
import Restrict from "components/Restrict.jsx";

class Nav extends Component {
  render() {
    return (
      <Restrict roles="admin">
        <Button.Group basic size="tiny" floated="left">
          <Button icon as={Link} to="/users/edit">
            <Icon name="plus" /> Novo usuário
          </Button>
        </Button.Group>
      </Restrict>
    );
  }
}

export default Nav;
