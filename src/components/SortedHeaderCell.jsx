import React from "react";
import { Table, Icon } from "semantic-ui-react";

export default class SortedHeaderCell extends React.Component {
  render() {
    return (
      <Table.HeaderCell {...this.props}>
        {this.props.children}{" "}
        {this.props.onClick && !this.props.sorted ? (
          <Icon name="sort" style={{ color: "#aaa" }} />
        ) : (
          ""
        )}
      </Table.HeaderCell>
    );
  }
}
