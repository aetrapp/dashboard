import _ from "underscore";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Table, Header, Label, Button, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";
import TrapsModeration from "components/TrapsModeration.jsx";
import moment from "moment";
import client, { apiUrl } from "services/feathers";
import TrapsFilter from "components/TrapsFilter.jsx";
import SortedHeaderCell from "components/SortedHeaderCell.jsx";
import Restrict from "components/Restrict.jsx";
import { getStatus, getAddressString } from "lib/traps";

class ListTraps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      traps: [],
      clicked: {},
      editing: {}
    };
    this._handleModeration = this._handleModeration.bind(this);
    this._userCanEdit = this._userCanEdit.bind(this);
  }
  componentDidMount() {
    const { traps } = this.props;
    this.setState({ traps });
  }
  componentWillReceiveProps(nextProps) {
    const { traps } = this.props;
    if (JSON.stringify(traps) !== JSON.stringify(nextProps.traps)) {
      this.setState({ traps: nextProps.traps });
    }
  }
  _trapLastCount(trap) {
    if (trap.eggCountSeries && trap.eggCountSeries.length) {
      return trap.eggCountSeries[trap.eggCountSeries.length - 1].eggCount;
    } else {
      return "-";
    }
  }
  _userCanEdit(trap) {
    const { auth } = this.props;
    if (!auth.signedIn) return false;
    if (auth.user.roles && auth.user.roles.indexOf("admin") !== -1) {
      return true;
    } else if (
      auth.user.roles &&
      auth.user.roles.indexOf("moderator") !== -1 &&
      auth.user.cities &&
      auth.user.cities.length
    ) {
      const ids = auth.user.cities.map(city => city.id);
      return ids.indexOf(trap.addressCityId) !== -1;
    }
  }
  _handleModeration(type, data) {
    let traps = this.state.traps.slice(0);
    switch (type) {
      case "removed":
        traps = traps.filter(trap => trap.id != data);
        break;
      case "updated":
        traps = traps.map(trap => {
          if (trap.id == data.id) {
            trap = data;
          }
        });
        break;
    }
    this.setState({ traps });
  }
  _lastSample(trap) {
    if (trap.samples && trap.samples.length) {
      const sample = trap.samples[0];
      if (sample.status == "valid") {
        return trap.samples[0].eggCount + " ovos";
      } else {
        return "Amostra inválida";
      }
    } else {
      return "--";
    }
  }
  _getNextSampleDate(trap) {
    return moment(trap.cycleStart)
      .add(trap.cycleDuration + 1, "days")
      .fromNow();
  }
  _getSort = field => {
    const { sort } = this.props;
    const current = Object.keys(sort)[0];
    if (field == current) {
      if (sort[current] == 1) {
        return "descending";
      } else {
        return "ascending";
      }
    }
    return null;
  };
  _handleSortClick = key => ev => {
    const { onSort } = this.props;
    const { clicked } = this.state;
    if (onSort) {
      const keyClicked = (clicked[key] || 0) + 1;
      const reset = keyClicked % 3 == 0;
      if (!reset) {
        onSort(key)();
      } else {
        onSort("")();
      }
      this.setState({
        clicked: {
          ...clicked,
          [key]: reset ? 0 : keyClicked
        }
      });
    }
  };
  render() {
    const { auth, showUser } = this.props;
    const { traps, editing } = this.state;
    if (traps.length) {
      return (
        <Table sortable>
          <Table.Header>
            <Table.Row>
              <SortedHeaderCell>ID</SortedHeaderCell>
              <SortedHeaderCell
                onClick={this._handleSortClick("city")}
                sorted={this._getSort("city")}
              >
                Município
              </SortedHeaderCell>
              <SortedHeaderCell>Bairro</SortedHeaderCell>
              <SortedHeaderCell>Endereço</SortedHeaderCell>
              <SortedHeaderCell
                onClick={this._handleSortClick("sampleCount")}
                sorted={this._getSort("sampleCount")}
              >
                Número de amostras
              </SortedHeaderCell>
              <SortedHeaderCell
                onClick={this._handleSortClick("eggCount")}
                sorted={this._getSort("eggCount")}
              >
                Última amostra (nº de ovos)
              </SortedHeaderCell>
              <SortedHeaderCell
                onClick={this._handleSortClick("cycleStart")}
                sorted={this._getSort("cycleStart")}
              >
                Próxima coleta
              </SortedHeaderCell>
              <SortedHeaderCell
                onClick={this._handleSortClick("status")}
                sorted={this._getSort("status")}
              >
                Situação
              </SortedHeaderCell>
              {showUser &&
              auth.signedIn &&
              auth.user.roles.indexOf("admin") !== -1 ? (
                <SortedHeaderCell>Usuário</SortedHeaderCell>
              ) : null}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {traps.map(trap => (
              <Table.Row key={trap.id}>
                <Table.Cell singleLine>
                  <Link to={`/traps/${trap.id}`}>{trap.id}</Link>{" "}
                  <Restrict
                    roles={["admin", "moderator"]}
                    cityRestrict={trap.city.id}
                  >
                    <span>
                      (<Link to={`/traps/edit/${trap.id}`}>editar</Link>)
                    </span>
                  </Restrict>
                </Table.Cell>
                <Table.Cell>
                  {trap.city ? trap.city.name : ""}
                  {trap.city ? ` - ${trap.city.stateId}` : ""}
                </Table.Cell>
                <Table.Cell>{trap.neighbourhood}</Table.Cell>
                <Table.Cell>{getAddressString(trap)}</Table.Cell>
                <Table.Cell>{trap.sampleCount} amostra(s)</Table.Cell>
                <Table.Cell>
                  {trap.eggCount != null ? (
                    <span>
                      {trap.eggCount}{" "}
                      {trap.eggCountDate ? (
                        <Label size="tiny">
                          {moment(new Date()).format("L")}
                        </Label>
                      ) : null}
                    </span>
                  ) : (
                    <span>--</span>
                  )}
                </Table.Cell>
                <Table.Cell>{this._getNextSampleDate(trap)}</Table.Cell>
                <Table.Cell>{getStatus(trap.status)}</Table.Cell>
                {showUser &&
                auth.signedIn &&
                auth.user.roles.indexOf("admin") !== -1 ? (
                  <Table.Cell>
                    <Link to={`/users/${trap.ownerId}`}>
                      {trap.owner ? (
                        <span>
                          {trap.owner.firstName} {trap.owner.lastName}
                        </span>
                      ) : (
                        <span>{trap.ownerId}</span>
                      )}
                    </Link>
                  </Table.Cell>
                ) : null}
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      );
    } else {
      return <Header as="h3">Nenhuma armadilha foi encontrada.</Header>;
    }
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps)(ListTraps);
