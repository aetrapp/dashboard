import React, { Component } from "react";
import { connect } from "react-redux";
import { logout } from "actions/auth";
import { withRouter, NavLink } from "react-router-dom";
import { Menu, Icon } from "semantic-ui-react";
import Restrict from "./Restrict.jsx";

class Nav extends Component {
  render() {
    const { auth, logout } = this.props;
    return (
      <Menu secondary>
        <Menu.Item>
          <a href="https://www.aetrapp.org/">
            <img className="ui" src={require("images/logo.png")} />
          </a>
        </Menu.Item>
        <Menu.Item as={NavLink} to="/" exact name="Home">
          <Icon name="map" />
          Mapa
        </Menu.Item>
        <Restrict roles="admin">
          <Menu.Item as={NavLink} to="/users" name="Users">
            <Icon name="users" />
            Usuários
          </Menu.Item>
        </Restrict>
        <Menu.Item as={NavLink} to="/traps" name="traps">
          <Icon name="radio" />
          Armadilhas
        </Menu.Item>
        <Restrict roles={["admin"]}>
          <Menu.Item as={NavLink} to="/samples" name="Samples">
            <Icon name="table" />
            Amostras
          </Menu.Item>
        </Restrict>
        <Restrict roles={["admin", "moderator"]}>
          <Menu.Item as={NavLink} to="/broadcast" name="Broadcast">
            <Icon name="announcement" />
            Broadcast
          </Menu.Item>
        </Restrict>
        <Menu.Menu position="right">
          {auth.signedIn ? (
            <Menu.Item name="Logout" onClick={logout}>
              Olá, {auth.user.firstName} {auth.user.lastName}{" "}
              <Icon name="sign out" style={{ marginLeft: "1.5rem" }} />
              Sair
            </Menu.Item>
          ) : (
            <Menu.Item as={NavLink} to="/login" name="Login">
              <Icon name="sign in" />
              Entrar
            </Menu.Item>
          )}
        </Menu.Menu>
      </Menu>
    );
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = {
  logout
};

export default withRouter(
  connect(
    mapStateToProps,
    mapDispatchToProps
  )(Nav)
);
