import React from "react";
import FileSaver from "file-saver";
import { Button } from "semantic-ui-react";
import { stringify } from "qs";
import axios from "axios";
import { apiUrl } from "services/feathers";
import { connect } from "react-redux";
import { addNotification } from "actions/notifications";

class FilteredDownload extends React.Component {
  constructor(props) {
    super(props);
    this._handleClick = this._handleClick.bind(this);
  }
  _getQuery() {
    const query = { ...this.props.query };
    delete query.$limit;
    delete query.$sort;
    return query;
  }
  _getUrl() {
    const { service } = this.props;
    const query = this._getQuery();
    return `${apiUrl}/downloads/${service}.csv?${stringify(query)}`;
  }
  _hasFilter() {
    if (Object.keys(this._getQuery()).length) {
      return true;
    }
    return false;
  }
  _handleClick(ev) {
    ev.preventDefault();
    const { service, addNotification } = this.props;
    const BOM = "\uFEFF";
    axios
      .get(this._getUrl(), {
        headers: {
          Authorization: window.localStorage["feathers-jwt"]
        }
      })
      .then(res => {
        const blob = new Blob([BOM + res.data], {
          type: "text/csv;charset=utf-8"
        });
        FileSaver.saveAs(blob, `${service}.csv`);
      })
      .catch(err => {
        addNotification({
          message: "Erro: " + err.response.statusText || err.message,
          level: "error"
        });
      });
  }
  render() {
    const { query, service, children, addNotification, ...props } = this.props;
    if (this._hasFilter()) {
      return (
        <Button {...props} onClick={this._handleClick} href={this._getUrl()}>
          {children}
        </Button>
      );
    } else {
      return null;
    }
  }
}

const mapDispatchToProps = {
  addNotification
};

export default connect(null, mapDispatchToProps)(FilteredDownload);
