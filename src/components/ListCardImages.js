import React, { Component } from 'react';
import client from 'services/feathers';
import { Card, Header } from 'semantic-ui-react';

class ListCardImages extends Component {
  constructor (props) {
    super(props);
    this.state = {
      cardImages: []
    };
  }
  componentDidMount () {
    client.service('images').find().then(res => {
      this.setState({
        cardImages: res.data
      });
    }).catch(err => {
      console.log(err);
    });
  }
  render () {
    const { cardImages } = this.state;
    if(cardImages.length) {
      return (
        <Card.Group>
          {cardImages.map(image => (
            <Card key={image.id}>
              <Card.Content>
                <Card.Header>
                  {image.id}
                </Card.Header>
                <Card.Description>
                  <p>{image.createdAt}</p>
                  <p>Egg count: {image.eggCount || 0}</p>
                </Card.Description>
              </Card.Content>
            </Card>
          ))}
        </Card.Group>
      )
    } else {
      return (
        <Header as="h3">No images were found.</Header>
      );
    }
  }
};

export default ListCardImages;
