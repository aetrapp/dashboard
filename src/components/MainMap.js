import _ from "underscore";
import client from "services/feathers";
import React, { Component } from "react";
import PropTypes from "prop-types";
import moment from "moment";
moment.locale("pt-br");
import { Map, Popup, TileLayer, LayerGroup } from "react-leaflet";
import { Marker, GeoJSON, Widget, Legend, ManageLayers } from "components/map";
import { withRouter, Link } from "react-router-dom";
import {
  Header,
  Button,
  Icon,
  Divider,
  Dimmer,
  Loader,
  Checkbox
} from "semantic-ui-react";
import TrapsFilter from "components/TrapsFilter.jsx";
import DownloadButtons from "components/DownloadButtons.jsx";
import FilteredDownload from "components/FilteredDownload.jsx";
import Restrict from "components/Restrict.jsx";
import {
  statuses,
  getStatus,
  getStatusColor,
  getAddressString
} from "lib/traps";

import L from "leaflet";

class RouterForwarder extends Component {
  getChildContext() {
    return this.props.context;
  }

  render() {
    return <span>{this.props.children}</span>;
  }
}

RouterForwarder.childContextTypes = {
  router: PropTypes.object.isRequired
};

RouterForwarder.propTypes = {
  context: PropTypes.object.isRequired
};

class MainMap extends Component {
  static contextTypes = {
    router: PropTypes.object.isRequired
  };
  constructor(props) {
    super(props);
    this.state = {
      loading: false,
      loadingLayers: false,
      position: [51.505, -0.09],
      layers: [],
      hiddenLayers: {},
      traps: [],
      bounds: [],
      query: {
        status: {
          $in: Object.keys(statuses).filter(status => status !== "inactive")
        },
        $sort: {
          createdAt: -1
        },
        $limit: 100
      },
      total: 0,
      hasMore: false,
      page: 1
    };
    this._updateTraps = this._updateTraps.bind(this);
    this._handleLegendClick = this._handleLegendClick.bind(this);
  }
  componentDidMount() {
    this._updateTraps();
    this._updateLayers();
  }
  componentDidUpdate(prevProps, prevState) {
    const { query } = this.state;
    if (JSON.stringify(query) !== JSON.stringify(prevState.query)) {
      this._updateTraps();
    }
  }
  _updateLayers() {
    this.setState({ loadingLayers: true });
    let layers = [];
    layers = [
      // {
      //   id: 1,
      //   title: "Teste",
      //   description: "Teste",
      //   geojson: JSON.parse(require("sample.geojson"))
      // }
    ];
    client
      .service("layers")
      .find({})
      .then(res => {
        if (res.data.length) {
          layers = res.data;
        }
        this.setState({
          loadingLayers: false,
          layers
        });
      })
      .catch(err => {
        this.setState({ loadingLayers: false });
      });
  }
  _toggleMapLayer = layerId => () => {
    const { hiddenLayers } = this.state;
    this.setState({
      hiddenLayers: {
        ...hiddenLayers,
        [layerId]: !hiddenLayers[layerId]
      }
    });
  };
  _isLayerHidden = layer => {
    const { hiddenLayers } = this.state;
    return hiddenLayers[layer.id];
  };
  _handleLayersChange = layers => {
    this.setState({ layers });
  };
  _updateTraps() {
    const { query } = this.state;
    let bounds = this.state.bounds.slice(0);
    this.setState({ loading: true });
    client
      .service("traps")
      .find({ query })
      .then(res => {
        if (res.data.length) {
          bounds = res.data.map(trap => trap.coordinates.coordinates);
        }
        this.setState({
          loading: false,
          page: 1,
          total: res.total,
          traps: res.data,
          bounds
        });
      })
      .catch(err => {
        this.setState({ loading: false });
      });
  }
  _handleFilter = _.debounce(filter => {
    const { query } = this.state;
    let newQuery = Object.assign({}, query);
    delete newQuery["$or"];
    delete newQuery["cityId"];
    newQuery.status = {
      $in: Object.keys(statuses).filter(status => status !== "inactive")
    };
    if (filter.q) {
      newQuery["$or"] = {};
      newQuery["$or"]["addressStreet"] = { $iLike: `%${filter.q}%` };
      newQuery["$or"]["id"] = { $iLike: `%${filter.q}%` };
    }
    if (filter.status) {
      newQuery["status"] = filter.status;
    }
    if (filter.cities && filter.cities.length) {
      newQuery["cityId"] = { $in: filter.cities };
    }
    this.setState({ query: newQuery });
  }, 200);

  _trapLastCount(trap) {
    if (trap.lastSample) {
      return trap.lastSample.eggCount || "0";
    }
    return trap.eggCount || "0";
  }

  _trapLastDate(trap) {
    if (trap.lastSample) {
      return moment(trap.lastSample.collectedAt).fromNow();
    } else {
      return null;
    }
  }

  _handleLegendClick(status) {
    const { query } = this.state;
    if (typeof query.status == "string" || !query.status.$in) {
      this.setState({
        query: {
          ...query,
          status: { $in: [status] }
        }
      });
    } else {
      const statuses = query.status.$in;
      if (statuses.indexOf(status) == -1) {
        this.setState({
          query: {
            ...query,
            status: { $in: [...statuses, status] }
          }
        });
      } else {
        const newStatuses = statuses.filter(s => s !== status);
        if (newStatuses.length) {
          this.setState({
            query: {
              ...query,
              status: { $in: newStatuses }
            }
          });
        } else {
          this.setState({
            query: {
              ...query,
              status: {
                $in: Object.keys(statuses).filter(
                  status => status !== "inactive"
                )
              }
            }
          });
        }
      }
    }
  }

  render() {
    const {
      loading,
      loadingLayers,
      layers,
      hiddenLayers,
      traps,
      bounds,
      query,
      total
    } = this.state;
    return (
      <div>
        <TrapsFilter onChange={this._handleFilter} />
        <DownloadButtons />
        <FilteredDownload
          service="traps"
          query={query}
          basic
          size="tiny"
          icon
          floated="right"
          style={{ marginRight: "1rem" }}
        >
          <Icon name="download" /> Baixar resultados filtrados
        </FilteredDownload>
        <Button
          floated="right"
          basic
          size="tiny"
          icon
          onClick={this._updateTraps}
          style={{ marginRight: "1rem" }}
        >
          <Icon name="refresh" loading={loading} /> Atualizar resultados
        </Button>
        <p
          style={{
            float: "left",
            lineHeight: "30px",
            margin: 0
          }}
        >
          {total} armadilhas encontradas.
        </p>
        <Divider hidden clearing />
        <Map
          center={[0, 0]}
          zoom={2}
          bounds={bounds.length ? bounds : undefined}
          scrollWheelZoom={false}
          style={{
            width: "100%",
            height: "600px"
          }}
        >
          <Dimmer active={loading} inverted>
            <Loader />
          </Dimmer>
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="&copy; <a href=&quot;https://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          />
          {layers.map(layer => {
            if (!this._isLayerHidden(layer)) {
              return <GeoJSON key={layer.id} data={layer.simplifiedGeojson} />;
            }
            return null;
          })}
          <LayerGroup>
            {traps.map(trap => (
              <Marker
                key={trap.id}
                color={getStatusColor(trap.status)}
                symbol="circle"
                position={trap.coordinates.coordinates.reverse()}
              >
                <Popup maxWidth="200">
                  <RouterForwarder context={this.context}>
                    <div className="sample-popup">
                      <Header as="h4">{getAddressString(trap)}</Header>
                      {trap.lastSample ? (
                        <p>
                          {this._trapLastCount(trap)} ovos encontrados na última
                          amostra, realizada há {this._trapLastDate(trap)}
                        </p>
                      ) : (
                        <p>Amostra não registrada.</p>
                      )}
                      <Button primary fluid as={Link} to={`/traps/${trap.id}`}>
                        Ver armadilha
                      </Button>
                    </div>
                  </RouterForwarder>
                </Popup>
              </Marker>
            ))}
          </LayerGroup>
          <Widget>
            <Restrict roles={["admin"]}>
              <div>
                <ManageLayers onChange={this._handleLayersChange} />
              </div>
            </Restrict>
            {layers.length ? (
              <div>
                <Button.Group vertical fluid basic size="tiny">
                  {layers.map(layer => (
                    <Button
                      key={layer.id}
                      onClick={this._toggleMapLayer(layer.id)}
                    >
                      <Checkbox
                        // toggle
                        fitted
                        checked={!this._isLayerHidden(layer)}
                        readOnly={true}
                        label={layer.title}
                      />
                    </Button>
                  ))}
                </Button.Group>
              </div>
            ) : null}
            {!layers.length && loadingLayers ? (
              <div>
                <Button icon fluid basic size="tiny">
                  <Icon name="spinner" loading /> Carregando camadas...
                </Button>
              </div>
            ) : null}
            <Legend onClick={this._handleLegendClick} active={query.status} />
          </Widget>
        </Map>
      </div>
    );
  }
}

export default withRouter(MainMap);
