import _ from "underscore";
import React, { Component } from "react";
import { connect } from "react-redux";
import { Card, Image, Header, Label, Button, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";
import TrapsModeration from "components/TrapsModeration.jsx";
import moment from "moment";
import client, { apiUrl } from "services/feathers";
import TrapsFilter from "components/TrapsFilter.jsx";

class ListTraps extends Component {
  constructor(props) {
    super(props);
    this.state = {
      traps: [],
      editing: {}
    };
    this._handleModeration = this._handleModeration.bind(this);
    this._userCanEdit = this._userCanEdit.bind(this);
  }
  componentDidMount() {
    const { traps } = this.props;
    this.setState({ traps });
  }
  componentWillReceiveProps(nextProps) {
    const { traps } = this.props;
    if (JSON.stringify(traps) !== JSON.stringify(nextProps.traps)) {
      this.setState({ traps: nextProps.traps });
    }
  }
  _trapLastCount(trap) {
    if (trap.eggCountSeries && trap.eggCountSeries.length) {
      return trap.eggCountSeries[trap.eggCountSeries.length - 1].eggCount;
    } else {
      return "-";
    }
  }
  _userCanEdit(trap) {
    const { auth } = this.props;
    if (!auth.signedIn) return false;
    if (auth.user.roles && auth.user.roles.indexOf("admin") !== -1) {
      return true;
    } else if (
      auth.user.roles &&
      auth.user.roles.indexOf("moderator") !== -1 &&
      auth.user.cities &&
      auth.user.cities.length
    ) {
      const ids = auth.user.cities.map(city => city.id);
      return ids.indexOf(trap.cityId) !== -1;
    }
  }
  _handleModeration(type, data) {
    let traps = this.state.traps.slice(0);
    switch (type) {
      case "removed":
        traps = traps.filter(trap => trap.id != data);
        break;
      case "updated":
        traps = traps.map(trap => {
          if (trap.id == data.id) {
            trap = data;
          }
        });
        break;
    }
    this.setState({ traps });
  }
  render() {
    const { traps, editing } = this.state;
    if (traps.length) {
      return (
        <Card.Group itemsPerRow={3}>
          {traps.map(trap => (
            <Card key={trap.id}>
              <Card.Content>
                <Card.Header>
                  <Link to={`/traps/${trap.id}`}>
                    {trap.addressStreet}, {trap.addressNumber}
                  </Link>
                  <Label
                    attached="top right"
                    color={trap.isActive ? "green" : null}
                    size="small"
                  >
                    {trap.isActive ? "Ativa" : "Concluída"}
                  </Label>
                </Card.Header>
                <Card.Meta>
                  {trap.city.name}, {trap.city.stateId}
                  <br />
                  cadastrada {moment(trap.createdAt).fromNow()}
                </Card.Meta>
                <Card.Description />
              </Card.Content>
              <Card.Content>
                <strong>Última amostra</strong>
                {trap.eggCountSeries && trap.eggCountSeries.length ? (
                  <p>
                    {this._trapLastCount(trap)} ovos encontrados na última
                    amostra
                  </p>
                ) : (
                  <p>Amostra não registrada.</p>
                )}
              </Card.Content>
              {this._userCanEdit(trap) ? (
                <Card.Content extra>
                  <TrapsModeration
                    trap={trap}
                    useToggle
                    onChange={this._handleModeration}
                  />
                </Card.Content>
              ) : null}
            </Card>
          ))}
        </Card.Group>
      );
    } else {
      return <Header as="h3">Nenhuma armadilha foi encontrada.</Header>;
    }
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

export default connect(mapStateToProps)(ListTraps);
