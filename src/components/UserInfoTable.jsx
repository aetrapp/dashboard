import React from "react";
import { Table } from "semantic-ui-react";
import moment from "moment";

export default class UserInfoTable extends React.Component {
  static meta = [
    {
      key: "email",
      label: "Email"
    },
    {
      key: "cellphoneNumber",
      label: "Celular"
    },
    {
      key: "landlineNumber",
      label: "Telefone"
    },
    {
      key: "gender",
      label: "Gênero",
      options: {
        male: "Masculino",
        female: "Feminino"
      }
    },
    {
      key: "dateOfBirth",
      label: "Data de nascimento",
      format: data => {
        if (data) {
          return moment(data).format("LL");
        }
      }
    },
    {
      key: "isActive",
      label: "Ativo",
      format: "bool"
    },
    {
      key: "trapCount",
      label: "Número de armadilhas",
      format: data => {
        if(data) {
          return data;
        } else {
          return 0;
        }
      }
    },
    {
      key: "delayedTrapCount",
      label: "Armadilhas atrasadas",
      format: data => {
        if(data) {
          return data;
        } else {
          return 0;
        }
      }
    },
    {
      key: "createdAt",
      label: "Data de registro",
      format: data => {
        if (data) {
          return moment(data).format("LL");
        }
      }
    }
  ];
  renderValue(val, item) {
    let result = val;
    if (item.options) {
      result = item.options[result];
    }
    if (item.format) {
      if (typeof item.format == "string") {
        switch (item.format) {
          case "bool":
            if (val) {
              result = "Sim";
            } else {
              result = "Não";
            }
            break;
        }
      } else if (typeof item.format == "function") {
        result = item.format(result);
      }
    }
    return result;
  }
  render() {
    const { user } = this.props;
    return (
      <Table definition>
        <Table.Body>
          {UserInfoTable.meta.map(item => (
            <Table.Row key={item.key}>
              <Table.Cell collapsing>{item.label}</Table.Cell>
              <Table.Cell>{this.renderValue(user[item.key], item)}</Table.Cell>
            </Table.Row>
          ))}
        </Table.Body>
      </Table>
    );
  }
}
