import React from "react";
import { connect } from "react-redux";
import NotificationSystem from "react-notification-system";

class Notifications extends React.Component {
  _notificationSystem = null;
  constructor(props) {
    super(props);
  }
  componentDidMount() {
    this._notificationSystem = this.refs.notificationSystem;
  }
  componentWillReceiveProps(nextProps) {
    const { notifications } = this.props;
    if (nextProps.notifications && nextProps.notifications !== notifications) {
      this._notificationSystem.addNotification(nextProps.notifications);
    }
  }
  render() {
    return <NotificationSystem ref="notificationSystem" />;
  }
}

const mapStateToProps = state => {
  return {
    notifications: state.notifications
  };
};

export default connect(mapStateToProps)(Notifications);
