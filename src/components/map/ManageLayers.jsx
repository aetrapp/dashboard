import React from "react";
import client, { apiUrl } from "services/feathers";
import { connect } from "react-redux";
import { addNotification } from "actions/notifications";
import {
  Modal,
  Table,
  Button,
  Icon,
  Header,
  Form,
  Input,
  TextArea,
  Divider
} from "semantic-ui-react";

class ManageLayers extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      loadingLayers: false,
      loading: false,
      layers: [],
      layer: {}
    };
    this.service = client.service("layers");
    this._handleChange = this._handleChange.bind(this);
    this._handleSubmit = this._handleSubmit.bind(this);
  }
  reset() {
    this.setState({
      loading: false,
      layers: [],
      layer: {}
    });
    this._updateLayers();
  }
  componentDidMount() {
    this.reset();
  }
  _handleModalOpen = () => {
    this.reset();
  };
  _updateLayers() {
    let layers = [];
    this.setState({ loadingLayers: true });
    layers = [
      // {
      //   id: 1,
      //   title: "Teste",
      //   description: "Teste",
      //   geojson: JSON.parse(require("sample.geojson"))
      // }
    ];
    client
      .service("layers")
      .find({})
      .then(res => {
        if (res.data.length) {
          layers = res.data;
        }
        this.setState({
          loadingLayers: false,
          layers
        });
      })
      .catch(err => {
        this.setState({ loadingLayers: false });
      });
  }
  _isGeoJSON(file) {
    if (!file) return false;
    if (file.type) {
      return file.type == "application/json" || file.type == "text/json";
    } else {
      return /\.json|.geojson$/.test(file.name);
    }
  }
  _handleChange(ev, { name, value }) {
    const { addNotification } = this.props;
    const { layer } = this.state;
    if (name == "geojson") {
      let file = ev.currentTarget.files[0];
      const reader = new FileReader();
      if (this._isGeoJSON(file)) {
        reader.onload = () => {
          this.setState({
            layer: {
              ...layer,
              [name]: JSON.parse(reader.result)
            }
          });
        };
        reader.readAsText(file, "utf-8");
      } else {
        this.setState({
          layer: {
            ...layer,
            [name]: null
          }
        });
        addNotification({
          message: "Formato não suportado",
          level: "error"
        });
      }
    } else {
      this.setState({
        layer: {
          ...layer,
          [name]: value
        }
      });
    }
  }
  _handleEditClick = layer => () => {
    this.setState({
      layer
    });
  };
  _handleResetLayer = () => {
    this.setState({
      layer: {
        title: "",
        description: "",
        geojson: {}
      }
    });
  };
  _handleRemoveClick = layer => () => {
    const { addNotification, onChange } = this.props;
    if (confirm("Você tem certeza?")) {
      this.service
        .remove(layer.id)
        .then(() => {
          let layers = [...this.state.layers];
          layers = layers.filter(l => l.id !== layer.id);
          if (onChange) onChange(layers);
          this.setState({ layers });
          addNotification({
            message: "Camada removida com sucesso",
            level: "success"
          });
        })
        .catch(err => {
          addNotification({
            message: "Erro: " + err.message,
            level: "error"
          });
        });
    }
  };
  _handleSubmit() {
    const { addNotification, onChange } = this.props;
    const { layer } = this.state;
    let req;
    if (layer.id) {
      delete layer.geojson;
      req = this.service.patch(layer.id, layer);
    } else {
      req = this.service.create(layer);
    }
    this.setState({
      loading: true
    });
    req
      .then(data => {
        let layers;
        let message;
        if (!layer.id) {
          layers = [...this.state.layers, data];
          message = "Camada criada com sucesso.";
          this.reset();
        } else {
          layers = [...this.state.layers].map(l => {
            if (l.id == data.id) {
              return data;
            }
            return l;
          });
          message = "Camada alterada com sucesso.";
        }
        this.setState({ loading: false, layers });
        if (onChange) onChange(layers);
        addNotification({
          message,
          level: "success"
        });
      })
      .catch(err => {
        this.setState({ loading: false });
        addNotification({
          message: "Erro: " + err.message,
          level: "error"
        });
      });
  }
  render() {
    const { loadingLayers, loading, layers, layer } = this.state;
    return (
      <Modal
        onOpen={this._handleModalOpen}
        trigger={
          <Button fluid icon size="tiny" secondary>
            <Icon name="cogs" /> Gerenciar camadas
          </Button>
        }
      >
        <Modal.Header>Gerenciar camadas</Modal.Header>
        <Modal.Content>
          <p>Gerencie camadas GeoJSON para exibição no mapa principal</p>
          {layers.length ? (
            <Table celled compact="very">
              <Table.Header>
                <Table.Row>
                  <Table.HeaderCell>Título</Table.HeaderCell>
                  <Table.HeaderCell>Descrição</Table.HeaderCell>
                  <Table.HeaderCell collapsing>Ações</Table.HeaderCell>
                </Table.Row>
              </Table.Header>
              <Table.Body>
                {layers.map(layer => (
                  <Table.Row key={layer.id}>
                    <Table.Cell>{layer.title}</Table.Cell>
                    <Table.Cell>{layer.description}</Table.Cell>
                    <Table.Cell>
                      <Button.Group basic size="tiny">
                        <Button onClick={this._handleEditClick(layer)}>
                          Editar
                        </Button>
                        <Button onClick={this._handleRemoveClick(layer)}>
                          Remover
                        </Button>
                      </Button.Group>
                    </Table.Cell>
                  </Table.Row>
                ))}
              </Table.Body>
            </Table>
          ) : null}
          {!layers.length && loadingLayers ? (
            <p>
              <Icon name="spinner" loading /> Carregando camadas...
            </p>
          ) : null}
          {layer.id ? (
            <div>
              <Button
                basic
                size="tiny"
                floated="right"
                onClick={this._handleResetLayer}
              >
                Criar nova camada
              </Button>
              <Header as="h3">Editando {layer.title}</Header>
            </div>
          ) : (
            <Header as="h3">Nova camada</Header>
          )}
          <Divider clearing />
          <Form onSubmit={this._handleSubmit}>
            <Form.Field
              control={Input}
              name="title"
              label="Título"
              onChange={this._handleChange}
              value={layer.title}
            />
            <Form.Field
              control={TextArea}
              name="description"
              label="Descrição"
              onChange={this._handleChange}
              value={layer.description}
            />
            {!layer.id ? (
              <Form.Field
                control={Input}
                type="file"
                name="geojson"
                label="Selecione o arquivo GeoJSON"
                onChange={this._handleChange}
              />
            ) : null}
            <Button primary fluid icon disabled={loading}>
              <Icon name={loading ? "spinner" : "save"} />{" "}
              {loading ? (
                <span>Enviando camada...</span>
              ) : (
                <span>Enviar camada</span>
              )}
            </Button>
          </Form>
        </Modal.Content>
      </Modal>
    );
  }
}

const mapDispatchToProps = {
  addNotification
};

export default connect(null, mapDispatchToProps)(ManageLayers);
