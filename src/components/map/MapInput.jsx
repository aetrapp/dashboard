import React, { Component } from "react";
import styled from "styled-components";
import { Map, Marker, TileLayer } from "react-leaflet";

const Wrapper = styled.div`
  .tip {
    color: #666;
    span {
      padding: 0.25rem 0.5rem;
      background: #f0f0f0;
      font-family: monospace;
      font-size: 0.8em;
      margin-left: 1rem;
    }
  }
`;

import L from "leaflet";
const imagePath = "/";
L.Icon.Default.imagePath = imagePath;
L.Icon.Default.mergeOptions({
  iconRetinaUrl: require("leaflet/dist/images/marker-icon-2x.png").split(
    imagePath
  )[1],
  iconUrl: require("leaflet/dist/images/marker-icon.png").split(imagePath)[1],
  shadowUrl: require("leaflet/dist/images/marker-shadow.png").split(
    imagePath
  )[1]
});

export default class MapInput extends Component {
  constructor(props) {
    super(props);
    this._handleDrag = this._handleDrag.bind(this);
  }
  _handleDrag(ev) {
    const { name, onChange } = this.props;
    if (onChange) {
      onChange(ev, {
        name,
        value: [ev.latlng.lng, ev.latlng.lat]
      });
    }
  }
  render() {
    const { value, height } = this.props;
    return (
      <Wrapper>
        <p className="tip">
          Arraste o marcador abaixo para alterar as coordenadas:{" "}
          <span>{value.join(", ")}</span>
        </p>
        <Map
          center={value ? [...value].reverse() : [0, 0]}
          zoom={15}
          scrollWheelZoom={false}
          style={{
            width: "100%",
            height: height || "300px"
          }}
        >
          <TileLayer
            url="https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png"
            attribution="&copy; <a href=&quot;https://osm.org/copyright&quot;>OpenStreetMap</a> contributors"
          />
          <Marker
            draggable
            onDrag={this._handleDrag}
            position={value ? [...value].reverse() : [0, 0]}
          />
        </Map>
      </Wrapper>
    );
  }
}
