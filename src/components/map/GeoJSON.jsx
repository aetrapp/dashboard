import React from "react";
import ReactDOM from "react-dom";
import L from "leaflet";
import MarkerClusterGroup from "react-leaflet-markercluster";
import simplestyle from "mapbox.js/src/simplestyle";
import { GeoJSON } from "react-leaflet";
import Marker from "./Marker.jsx";
import SimpleMarker from "./SimpleMarker.jsx";
import { Table } from "semantic-ui-react";

import "react-leaflet-markercluster/dist/styles.min.css";

const Fragment = React.Fragment;

export default class AeTrappGeoJSON extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      ready: false,
      clustered: []
    };
    this._add = this._add.bind(this);
  }
  componentDidMount() {
    this._handleMarkers();
  }
  componentWillReceiveProps() {
    this._handleMarkers();
  }
  _props(fp) {
    if (Array.isArray(fp)) fp = Object.assign({}, ...fp);
    fp = { ...(fp || {}) };
    delete fp["fill"];
    delete fp["fill-opacity"];
    delete fp["stroke"];
    delete fp["stroke-opacity"];
    delete fp["stroke-width"];
    delete fp["marker-color"];
    delete fp["marker-size"];
    delete fp["marker-symbol"];
    if (Object.keys(fp).length) {
      return this._table(fp);
    } else {
      return "";
    }
  }
  _table(p) {
    const keys = Object.keys(p);
    return (
      <Table definition compact="very" basic>
        <Table.Body>
          {keys.map(
            key =>
              p[key] ? (
                <Table.Row key={key}>
                  <Table.Cell>{key}</Table.Cell>
                  <Table.Cell>{p[key]}</Table.Cell>
                </Table.Row>
              ) : null
          )}
        </Table.Body>
      </Table>
    );
  }
  _popup(feature) {
    const content = this._props(feature.properties);
    if (content) {
      const div = document.createElement("div");
      ReactDOM.render(content, div);
      return L.popup({
        closeButton: false,
        maxWidth: 500,
        maxHeight: 400,
        autoPanPadding: [5, 45]
      }).setContent(div);
    }
    return false;
  }
  _add(feature, layer) {
    const popup = this._popup(feature);
    if (popup) {
      layer.bindPopup(popup);
    }
  }
  _handleMarkers() {
    const { data } = this.props;
    const points = data.features.filter(f => f.geometry.type == "Point");
    if (points.length > 200) {
      this.setState({
        clustered: points,
        ready: true
      });
    } else {
      this.setState({
        clustered: [],
        ready: true
      });
    }
  }
  _handleSimpleMarkerClick = ev => {
    const feature = ev.target.options.data;
    const popup = this._popup(feature);
    if (popup && !ev.target._popup) {
      ev.target.bindPopup(popup).openPopup();
    }
  };
  render() {
    const { ready, clustered } = this.state;
    const { data } = this.props;
    if (ready) {
      return (
        <Fragment>
          <GeoJSON
            data={data}
            style={simplestyle.style}
            pointToLayer={(feature, latlon) => {
              if (clustered.length) {
                return;
              }
              if (!feature.properties) feature.properties = {};
              return Marker.marker(feature, latlon);
            }}
            onEachFeature={this._add}
          />
          {clustered.length ? (
            <MarkerClusterGroup>
              {clustered.map((f, i) => (
                <SimpleMarker
                  key={i}
                  data={f}
                  position={[
                    f.geometry.coordinates[1],
                    f.geometry.coordinates[0]
                  ]}
                  onClick={this._handleSimpleMarkerClick}
                />
              ))}
            </MarkerClusterGroup>
          ) : null}
        </Fragment>
      );
    } else {
      return null;
    }
  }
}
