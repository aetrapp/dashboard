import React from "react";
import { statuses, getStatusColor } from "lib/traps";
import styled from "styled-components";

const Wrapper = styled.div`
  border: 1px solid #ccc;
  &.interactive {
    p {
      &:first-child {
        border-radius: 4px 4px 0 0;
      }
      &:last-child {
        border-radius: 0 0 4px 4px;
      }
      cursor: pointer;
      &:hover {
        background: #f0f0f0;
      }
      &.active {
        background: #333;
        color: #fff;
      }
    }
  }
  p {
    line-height: 20px;
    padding: 0.5rem 1rem;
    margin: 0;
    border-bottom: 1px solid #f0f0f0;
    &:last-child {
      border: 0;
    }
    span {
      float: left;
      margin-right: 1rem;
      svg {
        width: 20px;
      }
    }
  }
`;

export default class MapLegend extends React.Component {
  constructor(props) {
    super(props);
    this._handleClick = this._handleClick.bind(this);
  }
  _handleClick = status => ev => {
    ev.preventDefault();
    const { onClick } = this.props;
    if (onClick) {
      onClick(status);
    }
  };
  _isActive = key => {
    const { active } = this.props;
    if (typeof active == "string") {
      return active == key;
    } else {
      return active.$in && active.$in.find(s => s == key);
    }
  };
  render() {
    const { onClick } = this.props;
    return (
      <Wrapper className={onClick ? "interactive" : ""}>
        {Object.keys(statuses).map(key => (
          <p
            key={key}
            onClick={this._handleClick(key)}
            className={this._isActive(key) ? "active" : ""}
          >
            <span
              style={{ fill: getStatusColor(key) }}
              dangerouslySetInnerHTML={{
                __html: require("images/map-marker-circle.svg")
              }}
            />{" "}
            {statuses[key]}
          </p>
        ))}
      </Wrapper>
    );
  }
}
