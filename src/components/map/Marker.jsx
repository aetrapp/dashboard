import React from "react";
import L from "leaflet";
import _ from "underscore";
import { Marker } from "react-leaflet";

const symbols = ["basic", "circle"];

export default class AeTrappMarker extends React.Component {
  static icon(fp) {
    fp = fp || {};
    const sizes = {
      small: [20, 40],
      medium: [30, 60],
      large: [35, 70]
    };
    let size = fp["marker-size"] || fp["size"] || "medium",
      symbol = fp["marker-symbol"] || fp["symbol"] || "basic",
      color = fp["marker-color"] || fp["color"] || "#7e7e7e";

    if (!_.find(symbols, s => s == symbol)) {
      symbol = "basic";
    }

    return L.divIcon({
      html: `<div style="fill:${color}">${require(`images/map-marker-${symbol}.svg`)}</div>`,
      iconSize: sizes[size],
      iconAnchor: [sizes[size][0] / 2, sizes[size][1] / 2],
      popupAnchor: [0, -sizes[size][1] / 2],
      className: ""
    });
  }
  static marker(f, latlon) {
    return L.marker(latlon, {
      icon: AeTrappMarker.icon(f.properties),
      title: (f.properties && f.properties.title) || ""
    });
  }
  render() {
    const { children, position, ...props } = this.props;
    return (
      <Marker icon={AeTrappMarker.icon(props)} position={position}>
        {children}
      </Marker>
    );
  }
}
