import GeoJSON from "./GeoJSON.jsx";
import Legend from "./Legend.jsx";
import Marker from "./Marker.jsx";
import Widget from "./Widget.jsx";
import ManageLayers from "./ManageLayers.jsx";

module.exports = {
  GeoJSON,
  Legend,
  Marker,
  Widget,
  ManageLayers
};
