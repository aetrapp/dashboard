import React from "react";
import { statuses, getStatusColor } from "lib/traps";
import styled from "styled-components";

const Wrapper = styled.div`
  position: absolute;
  bottom: 0;
  left: 0;
  margin: 5px;
  z-index: 9999;
  > * {
    margin: 5px;
    background: #fff;
    border-radius: 5px;
  }
  p {
    line-height: 20px;
    padding: 0.5rem 1rem;
    margin: 0;
    border-bottom: 1px solid #f0f0f0;
    &:last-child {
      border: 0;
    }
    span {
      float: left;
      margin-right: 1rem;
      svg {
        width: 20px;
      }
    }
  }
`;

export default class MapWidget extends React.Component {
  render() {
    return <Wrapper>{this.props.children}</Wrapper>;
  }
}
