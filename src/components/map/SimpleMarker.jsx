import React from "react";
import L from "leaflet";
import _ from "underscore";
import { Marker } from "react-leaflet";

const symbols = ["basic", "circle"];

export default class AeTrappSimpleMarker extends React.Component {
  static icon(fp) {
    fp = fp || {};
    const sizes = {
      small: [10, 10],
      medium: [20, 20],
      large: [30, 30]
    };
    let size = fp["marker-size"] || fp["size"] || "small",
      color = fp["marker-color"] || fp["color"] || "#7e7e7e";

    return L.divIcon({
      html: `<div class="simple-marker" style="background-color:${color}"></div>`,
      iconSize: sizes[size],
      iconAnchor: [sizes[size][0] / 2, sizes[size][1] / 2],
      popupAnchor: [0, -sizes[size][1] / 2],
      className: ""
    });
  }
  static marker(f, latlon) {
    return L.marker(latlon, {
      icon: AeTrappSimpleMarker.icon(f.properties),
      title: (f.properties && f.properties.title) || ""
    });
  }
  render() {
    const { children, position, ...props } = this.props;
    return (
      <Marker
        icon={AeTrappSimpleMarker.icon(props)}
        position={position}
        {...props}
      >
        {children}
      </Marker>
    );
  }
}
