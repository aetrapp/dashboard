import React from "react";
import client from "services/feathers";
import { addNotification } from "actions/notifications";
import { connect } from "react-redux";
import { Button, Icon } from "semantic-ui-react";
import { Link } from "react-router-dom";

class TrapsModeration extends React.Component {
  constructor(props) {
    super(props);
    this.service = client.service("traps");
    this.state = {
      editing: !props.useToggle
    };
    this._userCanEdit = this._userCanEdit.bind(this);
    this._edit = this._edit.bind(this);
    this._activate = this._activate.bind(this);
    this._suspend = this._suspend.bind(this);
    this._remove = this._remove.bind(this);
  }
  _userCanEdit(trap) {
    const { auth } = this.props;
    if (!auth.signedIn) return false;
    if (auth.user.roles.indexOf("admin") !== -1) {
      return true;
    } else if (
      auth.user.roles.indexOf("moderator") !== -1 &&
      auth.user.cities &&
      auth.user.cities.length
    ) {
      const ids = auth.user.cities.map(city => city.id);
      return ids.indexOf(trap.cityId) !== -1;
    }
  }
  _edit(trapId) {
    this.setState({
      editing: true
    });
    setTimeout(() => {
      this.setState({
        editing: false
      });
    }, 2000);
  }
  _activate(trapId, isActive) {
    const { onChange, addNotification } = this.props;
    if (confirm("Você tem certeza?")) {
      let { traps } = this.state;
      this.service
        .patch(trapId, { isActive })
        .then(data => {
          if (onChange) {
            onChange("updated", data);
          }
          addNotification({
            message: "Armadilha alterada com sucesso.",
            level: "success"
          });
        })
        .catch(err => {
          addNotification({
            message: "Erro: " + err.message,
            level: "error"
          });
        });
    }
  }
  _suspend(trapId) {}
  _remove(trapId) {
    const { onChange, addNotification } = this.props;
    if (
      confirm(
        "Você tem certeza que deseja remover esta armadilha? Ela será apagada permanentemente do banco de dados."
      )
    ) {
      let { traps } = this.state;
      this.service
        .remove(trapId)
        .then(() => {
          if (onChange) {
            onChange("removed", trapId);
          }
          addNotification({
            message: "Armadilha removida com sucesso.",
            level: "success"
          });
        })
        .catch(err => {
          addNotification({
            message: "Erro: " + err.message,
            level: "error"
          });
        });
    }
  }

  render() {
    const { trap } = this.props;
    const { editing } = this.state;
    if (this._userCanEdit(trap)) {
      if (editing) {
        return (
          <div className="ui three buttons">
            <Button
              as={Link}
              basic
              icon
              color="grey"
              to={`/traps/edit/${trap.id}`}
            >
              <Icon name="edit" /> Editar
            </Button>
            <Button
              basic
              icon
              color="green"
              onClick={() => this._activate(trap.id, !trap.isActive)}
            >
              <Icon name="refresh" /> {trap.isActive ? "Concluir" : "Reiniciar"}
            </Button>
            {/* <Button
              basic
              icon
              color="yellow"
              onClick={() => this._suspend(trap.id)}
            >
              <Icon name="warning" /> Suspender
            </Button> */}
            <Button
              fluid
              basic
              icon
              color="red"
              onClick={() => this._remove(trap.id)}
            >
              <Icon name="trash" /> Remover
            </Button>
          </div>
        );
      } else {
        return (
          <Button fluid basic icon="edit" onClick={() => this._edit(trap.id)} />
        );
      }
    } else {
      return null;
    }
  }
}

const mapStateToProps = state => {
  return {
    auth: state.auth
  };
};

const mapDispatchToProps = {
  addNotification
};

export default connect(mapStateToProps, mapDispatchToProps)(TrapsModeration);
