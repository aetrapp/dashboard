import React from "react";
import styled from "styled-components";
import { Grid, Button } from "semantic-ui-react";

const Wrapper = styled.div`
  .image-container {
    width: 100%;
    min-height: 300px;
    box-sizing: border-box;
    border: 1px solid rgba(34, 36, 38, 0.15);
    padding: 0.5rem;
    border-radius: 0.28571429rem;
    img {
      display: block;
      max-width: 100%;
      max-height: 450px;
      margin: 0 auto;
    }
  }
`;

export default class ImageInput extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      key: 0
    };
    this.fileInput = null;
    this.setFileInputRef = element => {
      this.fileInput = element;
    };
    this._handleChange = this._handleChange.bind(this);
  }
  componentDidUpdate(prevProps, prevState) {
    const { onChange, name } = this.props;
    const { base64 } = this.state;
    if (onChange && base64 !== prevState.base64) {
      onChange(null, { name, value: base64 });
    }
  }
  _isImage(file) {
    if (!file) return false;
    if (file.type) {
      return file.type == "image/jpeg" || file.type == "image/png";
    } else {
      return /\.jpg|.jpeg|.png$/.test(file.name);
    }
  }
  _handleButtonClick = ev => {
    ev.preventDefault();
    this.fileInput.click();
  };
  _handleChange = ev => {
    const { key } = this.state;
    let file = ev.currentTarget.files[0];
    const reader = new FileReader();
    reader.onload = () => {
      this.setState({
        key: key + 1,
        base64: reader.result
      });
    };
    if (this._isImage(file)) {
      reader.readAsDataURL(file);
    }
  };
  render() {
    const { defaultValue } = this.props;
    const { key, base64 } = this.state;
    return (
      <Wrapper>
        <Grid widths="equal" columns="2">
          <Grid.Row>
            <Grid.Column>
              <div className="image-container">
                {base64 ? <img src={base64} /> : null}
                {defaultValue && !base64 ? <img src={defaultValue} /> : null}
              </div>
            </Grid.Column>
            <Grid.Column>
              <Button.Group>
                <Button color="green" onClick={this._handleButtonClick}>
                  Selecionar imagem
                </Button>
                {base64 ? (
                  <Button
                    onClick={ev => {
                      ev.preventDefault();
                      this.setState({ base64: "" });
                    }}
                  >
                    Remover imagem selecionada
                  </Button>
                ) : null}
              </Button.Group>
              <input
                style={{
                  display: "none"
                }}
                key={key}
                ref={this.setFileInputRef}
                type="file"
                onChange={this._handleChange}
              />
            </Grid.Column>
          </Grid.Row>
        </Grid>
      </Wrapper>
    );
  }
}
