import React from "react";
import moment from "moment";
import { Link } from "react-router-dom";
import { Table, Image } from "semantic-ui-react";
import { apiUrl } from "services/feathers";
import SortedHeaderCell from "components/SortedHeaderCell.jsx";

export default class SamplesTable extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      clicked: {}
    };
  }
  _status(status) {
    switch (status) {
      case "unprocessed":
        return "Não processada";
      case "invalid":
        return "Inválida";
      case "valid":
        return "Válida";
      case "analysing":
        return "Em análise";
    }
  }
  _handleSortClick = key => ev => {
    const { onSort } = this.props;
    const { clicked } = this.state;
    if (onSort) {
      const keyClicked = (clicked[key] || 0) + 1;
      const reset = keyClicked % 3 == 0;
      if (!reset) {
        onSort(key)();
      } else {
        onSort("")();
      }
      this.setState({
        clicked: {
          ...clicked,
          [key]: reset ? 0 : keyClicked
        }
      });
    }
  };
  _getSort = field => {
    const { sort } = this.props;
    const current = Object.keys(sort)[0];
    if (field == current) {
      if (sort[current] == 1) {
        return "descending";
      } else {
        return "ascending";
      }
    }
    return null;
  };
  render() {
    const { samples, full } = this.props;
    if (samples.length) {
      return (
        <Table sortable>
          <Table.Header>
            <Table.Row>
              {full ? <SortedHeaderCell collapsing>ID</SortedHeaderCell> : null}
              <SortedHeaderCell collapsing>Imagem</SortedHeaderCell>
              {full ? (
                <SortedHeaderCell collapsing>Usuário</SortedHeaderCell>
              ) : null}
              {full ? (
                <SortedHeaderCell collapsing>Armadilha</SortedHeaderCell>
              ) : null}
              {full ? (
                <SortedHeaderCell
                  collapsing
                  onClick={this._handleSortClick("city")}
                  sorted={this._getSort("city")}
                >
                  Município
                </SortedHeaderCell>
              ) : null}
              <SortedHeaderCell
                onClick={this._handleSortClick("collectedAt")}
                sorted={this._getSort("collectedAt")}
              >
                Data de amostra
              </SortedHeaderCell>
              <SortedHeaderCell
                onClick={this._handleSortClick("eggCount")}
                sorted={this._getSort("eggCount")}
              >
                Contagem de ovos
              </SortedHeaderCell>
              <SortedHeaderCell>Status</SortedHeaderCell>
              {full ? (
                <SortedHeaderCell>Mensagem de erro</SortedHeaderCell>
              ) : null}
            </Table.Row>
          </Table.Header>
          <Table.Body>
            {samples.map(sample => (
              <Table.Row key={sample.id}>
                {full ? <Table.Cell collapsing>{sample.id}</Table.Cell> : null}
                <Table.Cell collapsing>
                  <a href={`${apiUrl}/files/${sample.blobId}`} target="_blank">
                    <Image
                      size="small"
                      src={`${apiUrl}/files/${sample.blobId}`}
                    />
                  </a>
                </Table.Cell>
                {full ? (
                  <Table.Cell collapsing>
                    <Link to={`/users/${sample.ownerId}`}>
                      {sample.owner ? (
                        <span>
                          {sample.owner.firstName} {sample.owner.lastName}
                        </span>
                      ) : (
                        <span>{sample.ownerId}</span>
                      )}
                    </Link>
                  </Table.Cell>
                ) : null}
                {full ? (
                  <Table.Cell collapsing>
                    <Link to={`/traps/${sample.trapId}`}>{sample.trapId}</Link>
                  </Table.Cell>
                ) : null}
                {full ? (
                  <Table.Cell collapsing>
                    {sample.city ? sample.city.name : ""}
                    {sample.city ? ` - ${sample.city.stateId}` : ""}{" "}
                  </Table.Cell>
                ) : null}
                <Table.Cell>
                  {moment(sample.collectedAt).format("DD/MM/YYYY HH:mm")}
                </Table.Cell>
                <Table.Cell>{sample.eggCount}</Table.Cell>
                <Table.Cell>{this._status(sample.status)}</Table.Cell>
                {full ? (
                  <Table.Cell>
                    {sample.error ? sample.error.message : null}
                  </Table.Cell>
                ) : null}
              </Table.Row>
            ))}
          </Table.Body>
        </Table>
      );
    } else {
      return null;
    }
  }
}
