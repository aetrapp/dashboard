import React, { Component } from "react";
import { connect } from "react-redux";
import { withRouter, Route, Link, Switch } from "react-router-dom";
import {
  Container,
  Divider,
  Header,
  Menu,
  Icon,
  Dimmer,
  Loader
} from "semantic-ui-react";

import "semantic-ui-css/semantic.min.css";
import "leaflet/dist/leaflet.css";
import "react-datetime/css/react-datetime.css";

import Nav from "components/Nav";
import Footer from "components/Footer.jsx";
import Notifications from "components/Notifications.jsx";
import SignIn from "components/SignIn";

import Home from "scenes/home";
import Traps from "scenes/traps";
import Users from "scenes/users";
import NewBroadcast from "scenes/broadcast/New.jsx";
import Samples from "scenes/samples";

import "styles/global.css";

class Application extends Component {
  render() {
    const { loading } = this.props;
    if (loading) {
      return (
        <Dimmer active={loading}>
          <Loader size="large" />
        </Dimmer>
      );
    } else {
      return (
        <div>
          <Notifications />
          <Container style={{ minHeight: "400px" }}>
            <Divider hidden />
            <Nav />
            <Divider />
            <Switch>
              <Route exact path="/" component={Home} />
              <Route path="/traps" component={Traps} />
              <Route path="/users" component={Users} />
              <Route path="/broadcast" component={NewBroadcast} />
              <Route path="/samples" component={Samples} />
              <Route path="/login/verify/:verifyToken" component={SignIn} />
              <Route path="/login/reset/:resetToken" component={SignIn} />
              <Route path="/login" component={SignIn} />
            </Switch>
          </Container>
          <Divider hidden />
          <Footer />
        </div>
      );
    }
  }
}

const mapStateToProps = (state, ownProps) => {
  return {
    loading: state.auth.loading
  };
};

export default withRouter(connect(mapStateToProps)(Application));
