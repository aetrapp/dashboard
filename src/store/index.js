import { createStore, applyMiddleware, compose } from 'redux';
import middlewares from 'middlewares';
import rootReducer from 'reducers';

export default (initialState = {}) => {
  const store = createStore(
    rootReducer(),
    initialState,
    compose(
      applyMiddleware(...middlewares),
      window.devToolsExtension ? window.devToolsExtension() : f => f
    )
  );
  return store;
}
