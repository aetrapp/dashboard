const path = require("path");
const webpack = require("webpack");
const HTMLWebpackPlugin = require("html-webpack-plugin");

module.exports = {
  entry: {
    main: ["./src/index"]
  },
  resolve: {
    modules: ["src", "node_modules"]
  },
  output: {
    path: path.resolve("public"),
    publicPath: "/",
    filename: "[name]-[chunkhash].js"
  },
  plugins: [
    new webpack.DefinePlugin({
      "process.env": {
        NODE_ENV: JSON.stringify(process.env.NODE_ENV || ""),
        API_URL: JSON.stringify(process.env.API_URL || "http://localhost:3030"),
        SITE_URL: JSON.stringify(process.env.SITE_URL || "")
      }
    }),
    new webpack.ContextReplacementPlugin(/moment[\/\\]locale$/, /en|pt-br/),
    new HTMLWebpackPlugin({
      template: path.resolve("src", "index.html"),
      filename: "index.html",
      inject: "body"
    }),
    new HTMLWebpackPlugin({
      template: path.resolve("src", "index.html"),
      filename: "404.html",
      inject: "body"
    })
  ],
  module: {
    loaders: [
      {
        test: /\.jsx?/,
        loader: "babel-loader",
        query: {
          presets: ["es2015", "react"],
          plugins: [
            "transform-object-rest-spread",
            "syntax-class-properties",
            "transform-class-properties"
          ]
        },
        exclude: /node_modules/
      },
      { test: /\.json$/, loader: "json-loader" },
      {
        test: /\.css$/,
        loader: "style-loader!css-loader"
      },
      {
        test: /\.geojson|.json$/,
        loader: "raw-loader"
      },
      {
        test: /fonts\/(.*)\.(ttf|otf|eot|svg|woff(2)?)(\?[a-z0-9]+)?$/,
        loader: "file-loader?name=fonts/[name].[ext]&publicPath=/"
      },
      {
        test: /\.(png|jpg|gif|mp4)$/,
        loader: "file-loader"
      },
      {
        test: /\.svg$/,
        loader: "svg-inline-loader"
      }
    ]
  }
};
